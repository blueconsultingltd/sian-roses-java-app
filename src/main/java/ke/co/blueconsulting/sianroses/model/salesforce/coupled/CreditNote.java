package ke.co.blueconsulting.sianroses.model.salesforce.coupled;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import ke.co.blueconsulting.sianroses.model.BaseSalesforceModel;

import java.io.Serializable;
import java.util.Date;

public class CreditNote extends BaseSalesforceModel implements Serializable {

    private static final long serialVersionUID = 1527097474049L;

    @SerializedName("ERP_ID__c")
    @Expose
    private String erpId;

    @SerializedName("CreatedDate")
    @Expose
    private Date postingDate;

    @SerializedName("Due_Date__c")
    @Expose
    private String dueDate;

    @SerializedName("PO_Number__c")
    @Expose
    private String pONumber;

    @SerializedName("Invoice_No__c")
    @Expose
    private String invoiceNumber;

    @SerializedName("Invoice__c")
    @Expose
    private String invoiceEntry;

    @SerializedName("Farm__c")
    @Expose
    private String farm;

    @SerializedName("Reason")
    @Expose
    private String reason;

    @SerializedName("Associated_Cases_Packing_List_Items__r")
    @Expose
    private AssociatedCasesPackingListItems associatedCasesPackingListItems;

    @SerializedName("Packing_List_Invoice__r")
    @Expose
    private PackingListInvoice packingListInvoice;

    public String getErpId() {
        return erpId;
    }

    public void setErpId(String erpId) {
        this.erpId = erpId;
    }

    public Date getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(Date postingDate) {
        this.postingDate = postingDate;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getpONumber() {
        return pONumber;
    }

    public void setpONumber(String pONumber) {
        this.pONumber = pONumber;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceEntry() {
        return invoiceEntry;
    }

    public void setInvoiceEntry(String invoiceEntry) {
        this.invoiceEntry = invoiceEntry;
    }

    public String getFarm() {
        return farm;
    }

    public void setFarm(String farm) {
        this.farm = farm;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public AssociatedCasesPackingListItems getAssociatedCasesPackingListItems() {
        return associatedCasesPackingListItems;
    }

    public void setAssociatedCasesPackingListItems(AssociatedCasesPackingListItems associatedCasesPackingListItems) {
        this.associatedCasesPackingListItems = associatedCasesPackingListItems;
    }

    public PackingListInvoice getPackingListInvoice() {
        return packingListInvoice;
    }

    public void setPackingListInvoice(PackingListInvoice packingListInvoice) {
        this.packingListInvoice = packingListInvoice;
    }
}