package ke.co.blueconsulting.sianroses.model.salesforce.coupled;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import ke.co.blueconsulting.sianroses.model.BaseSalesforceModel;

public class PackingListInvoice extends BaseSalesforceModel {

    @SerializedName("Customer_Name__c")
    @Expose
    private String accountId;

    @SerializedName("Invoice__c")
    @Expose
    private String invoice;

    public PackingListInvoice(String accountId) {
        this.accountId = accountId;
    }

    public PackingListInvoice() {

    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }
}