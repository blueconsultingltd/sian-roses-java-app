package ke.co.blueconsulting.sianroses.model.salesforce.coupled;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import ke.co.blueconsulting.sianroses.model.BaseSalesforceModel;

import java.io.Serializable;
import java.util.ArrayList;

public class AssociatedCasesPackingListItems extends BaseSalesforceModel implements Serializable {

    @SerializedName("records")
    @Expose
    private ArrayList<CreditNoteItem> creditNoteItems = new ArrayList<>();

    public ArrayList<CreditNoteItem> getCreditNoteItems() {
        return creditNoteItems;
    }

    public void setCreditNoteItems(ArrayList<CreditNoteItem> creditNoteItems) {
        this.creditNoteItems = creditNoteItems;
    }


}