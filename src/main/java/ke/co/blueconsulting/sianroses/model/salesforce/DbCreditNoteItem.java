package ke.co.blueconsulting.sianroses.model.salesforce;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import ke.co.blueconsulting.sianroses.model.BaseSalesforceModel;

@DatabaseTable(tableName = "ARCREDITCH")
public class DbCreditNoteItem extends BaseSalesforceModel {

    @DatabaseField(columnName = "AUTOIDMST")
    private int autoIdMst;

    @DatabaseField(columnName = "Flower_Code__c")
    private String flowerCode;

    @DatabaseField(columnName = "Name")
    private String name;

    @DatabaseField(columnName = "Quantity__c")
    private double quantity;

    @DatabaseField(columnName = "Warehouse__c")
    private String warehouseC; //Missing

    @DatabaseField(columnName = "UnitPrice")
    private double unitPrice;

    @DatabaseField(columnName = "Tax__c")
    private String taxC;//Missing

    @DatabaseField(columnName = "Invoice_Entry__c")
    private String InvoiceEntry;

    public int getAutoIdMst() {
        return autoIdMst;
    }

    public void setAutoIdMst(int autoIdMst) {
        this.autoIdMst = autoIdMst;
    }

    public String getFlowerCode() {
        return flowerCode;
    }

    public void setFlowerCode(String flowerCode) {
        this.flowerCode = flowerCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getWarehouseC() {
        return warehouseC;
    }

    public void setWarehouseC(String warehouseC) {
        this.warehouseC = warehouseC;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getTaxC() {
        return taxC;
    }

    public void setTaxC(String taxC) {
        this.taxC = taxC;
    }

    public String getInvoiceEntry() {
        return InvoiceEntry;
    }

    public void setInvoiceEntry(String invoiceEntry) {
        InvoiceEntry = invoiceEntry;
    }
}
