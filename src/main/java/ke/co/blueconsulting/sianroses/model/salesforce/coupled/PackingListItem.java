package ke.co.blueconsulting.sianroses.model.salesforce.coupled;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import ke.co.blueconsulting.sianroses.model.BaseSalesforceModel;

public class PackingListItem extends BaseSalesforceModel {

    @SerializedName("List_Price__c")
    @Expose
    private double unitPrice;

    @SerializedName("Flower_Name__c")
    @Expose
    private String name;

    @SerializedName("Quantity__c")
    @Expose
    private double quantity;

    @SerializedName("Flower_Code__c")
    @Expose
    private String flowerCode;

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getFlowerCode() {
        return flowerCode;
    }

    public void setFlowerCode(String flowerCode) {
        this.flowerCode = flowerCode;
    }
}