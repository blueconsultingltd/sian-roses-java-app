package ke.co.blueconsulting.sianroses.model.salesforce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import ke.co.blueconsulting.sianroses.model.BaseSalesforceModel;

import java.util.Date;

/**
 * This model maps Salesforce Case Model SAP. Syncing is 2 ways from
 * Salesforce to SAP
 */
@DatabaseTable(tableName = "ARCREDIT")
public class DbCreditNote extends BaseSalesforceModel {

    @DatabaseField(columnName = "ERP_ID__c")
    @SerializedName("ERP_ID__c")
    @Expose
    private String erpId;//missing

    @DatabaseField(columnName = "AccountID")
    @SerializedName("Customer_Name__c")
    @Expose
    private String accountId;

    @DatabaseField(columnName = "Posting_Date__c")
    @SerializedName("CreatedDate")
    private Date postingDate;

    @DatabaseField(columnName = "Due_Date__c")
    @SerializedName("Due_Date__c")
    @Expose
    private String dueDate;

    @DatabaseField(columnName = "PO_Number__c")
    @SerializedName("PO_Number__c")
    @Expose
    private String pONumber;

    @DatabaseField(columnName = "Invoice_Number__c")
    @SerializedName("Invoice_No__c")
    @Expose
    private String invoiceNumber;

    @DatabaseField(columnName = "Invoice_Entry__c")
    @SerializedName("Invoice__c")
    @Expose
    private String invoiceEntry;

    @DatabaseField(columnName = "Farm_c")
    @SerializedName("Farm__c")
    @Expose
    private String farm;

    @DatabaseField(columnName = "Reason")
    @SerializedName("Reason")
    @Expose
    private String reason;


    public String getErpId() {
        return erpId;
    }

    public void setErpId(String erpId) {
        this.erpId = erpId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Date getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(Date postingDate) {
        this.postingDate = postingDate;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getpONumber() {
        return pONumber;
    }

    public void setpONumber(String pONumber) {
        this.pONumber = pONumber;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceEntry() {
        return invoiceEntry;
    }

    public void setInvoiceEntry(String invoiceEntry) {
        this.invoiceEntry = invoiceEntry;
    }

    public String getFarm() {
        return farm;
    }

    public void setFarm(String farm) {
        this.farm = farm;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
