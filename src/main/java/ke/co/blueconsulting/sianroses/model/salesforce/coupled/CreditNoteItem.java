package ke.co.blueconsulting.sianroses.model.salesforce.coupled;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import ke.co.blueconsulting.sianroses.model.BaseSalesforceModel;

import java.io.Serializable;

public class CreditNoteItem extends BaseSalesforceModel implements Serializable {

    private static final long serialVersionUID = 986369289632L;

    @SerializedName("Packing_List_Invoice__r")
    @Expose
    private PackingListInvoice packingListInvoice;

    @SerializedName("Packing_List_Item__r")
    @Expose
    private PackingListItem packingListItem;

    public PackingListItem getPackingListItem() {
        return packingListItem;
    }

    public void setPackingListItem(PackingListItem packingListItem) {
        this.packingListItem = packingListItem;
    }

    public PackingListInvoice getPackingListInvoice() {
        return packingListInvoice;
    }

    public void setPackingListInvoice(PackingListInvoice packingListInvoice) {
        this.packingListInvoice = packingListInvoice;
    }
}