package ke.co.blueconsulting.sianroses.data.sync;

import ke.co.blueconsulting.sianroses.contract.SyncContract;
import ke.co.blueconsulting.sianroses.data.DataService;
import ke.co.blueconsulting.sianroses.data.impl.SyncDataService;
import ke.co.blueconsulting.sianroses.data.impl.db.CustomerContactDbService;
import ke.co.blueconsulting.sianroses.model.app.Response;
import ke.co.blueconsulting.sianroses.model.salesforce.CustomerContact;
import ke.co.blueconsulting.sianroses.util.AppLogger;

import java.util.ArrayList;

import static ke.co.blueconsulting.sianroses.util.UpdateFields.updateContactSyncFields;

public class CustomerContacts {

    private final String TAG_PROCESS_NAME = "customer_contacts";
    private final SyncDataService syncDataService;
    private final CustomerContactDbService dbService;
    private final SyncContract.View syncDashboard;

    public CustomerContacts(SyncContract.View syncDashboard, SyncDataService syncDataService) {

        this.syncDashboard = syncDashboard;

        this.syncDataService = syncDataService;

        this.dbService = new CustomerContactDbService();
    }

    public void sync() {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        DataService.GetCallback<Response> getFromSalesforceCallback = new DataService.GetCallback<Response>() {

            @Override
            public void onCompleted(Response response) {

                ArrayList<CustomerContact> contacts = response.getCustomerContacts();

                AppLogger.log(TAG_PROCESS_NAME, "Fetch from Salesforce Successful. Received "
                        + contacts.size() + " contacts from Salesforce for upserting");

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                upsertSap(contacts);
            }

            @Override
            public void onError(Throwable e) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when fetching contact records from Salesforce. " + e.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

        };

        syncDataService.getContacts(getFromSalesforceCallback);

    }

    private void upsertSap(ArrayList<CustomerContact> contacts) {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        contacts = updateContactSyncFields(contacts, false, false);

        DataService.GetCallback<ArrayList<CustomerContact>> upsertContactsCallback = new DataService.GetCallback<ArrayList<CustomerContact>>() {

            @Override
            public void onCompleted(ArrayList<CustomerContact> contacts) {

                if (contacts.size() > 0) {
                    AppLogger.log(TAG_PROCESS_NAME, "Upserting on SAP successful");
                }

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                getUnsyncedContacts();

            }

            @Override
            public void onError(Throwable e) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when upserting contact records. " + e.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

        };

        dbService.upsertRecords(contacts, upsertContactsCallback);
    }

    private void getUnsyncedContacts() {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        DataService.GetCallback<ArrayList<CustomerContact>> getUnsyncedCustomersCallback = new DataService.GetCallback<ArrayList<CustomerContact>>() {

            @Override
            public void onCompleted(ArrayList<CustomerContact> contactArrayList) {

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                pushToSalesforce(contactArrayList);
            }

            @Override
            public void onError(Throwable e) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when fetching unsynced contact records from Sap. " + e.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);
            }

        };

        dbService.getUnsynced(getUnsyncedCustomersCallback);

    }

    private void pushToSalesforce(ArrayList<CustomerContact> contacts) {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        contacts = updateContactSyncFields(contacts, false, false);

        int contactsCount = contacts.size();

        AppLogger.log(TAG_PROCESS_NAME, "Found " + contactsCount + " contacts that need to be pushed to Salesforce. " + (contactsCount > 0 ? "Attempting to push." : ""));

        if (contactsCount > 0) {

            DataService.GetCallback<ArrayList<CustomerContact>> upsertCustomersCallback = new DataService.GetCallback<ArrayList<CustomerContact>>() {

                @Override
                public void onCompleted(ArrayList<CustomerContact> response) {

                    AppLogger.log(TAG_PROCESS_NAME, "Contacts sync complete");

                    syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                }

                @Override
                public void onError(Throwable e) {

                    AppLogger.log(TAG_PROCESS_NAME, "An error occurred when upserting contact records. " + e.getMessage());

                    syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                }

            };

            DataService.GetCallback<Response> pushToSalesforceCallback = new DataService.GetCallback<Response>() {

                @Override
                public void onCompleted(Response response) {

                    ArrayList<CustomerContact> contacts = response.getCustomerContacts();

                    int contactsCount = contacts.size();

                    AppLogger.log(TAG_PROCESS_NAME, "Push To Salesforce Successful. Received "
                            + contactsCount + " contacts from Salesforce for updating");

                    contacts = updateContactSyncFields(contacts, false, false);

                    dbService.upsertRecords(contacts, upsertCustomersCallback);

                }

                @Override
                public void onError(Throwable e) {

                    AppLogger.log(TAG_PROCESS_NAME, "An error occurred when pushing contact records to Salesforce. " + e.getMessage());

                    syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);
                }
            };

            syncDataService.pushCustomersContactsToSalesforce(Response.setCustomerContacts(contacts), pushToSalesforceCallback);

        } else {

            AppLogger.log(TAG_PROCESS_NAME, "Contacts sync complete");

            syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);
        }
    }
}
