package ke.co.blueconsulting.sianroses.data.sync;

import ke.co.blueconsulting.sianroses.contract.SyncContract;
import ke.co.blueconsulting.sianroses.data.DataService;
import ke.co.blueconsulting.sianroses.data.impl.SyncDataService;
import ke.co.blueconsulting.sianroses.data.impl.db.CustomerDbService;
import ke.co.blueconsulting.sianroses.model.app.Response;
import ke.co.blueconsulting.sianroses.model.salesforce.Customer;
import ke.co.blueconsulting.sianroses.util.AppLogger;

import java.util.ArrayList;

import static ke.co.blueconsulting.sianroses.util.UpdateFields.updateSyncFields;

public class Customers {

    private final String TAG_PROCESS_NAME = "customers";
    private final SyncDataService syncDataService;
    private final CustomerDbService dbService;
    private final SyncContract.View syncDashboard;

    public Customers(SyncContract.View syncDashboard, SyncDataService syncDataService) {

        this.syncDashboard = syncDashboard;

        this.syncDataService = syncDataService;

        this.dbService = new CustomerDbService();
    }

    public void sync() {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        DataService.GetCallback<Response> getFromSalesforceCallback = new DataService.GetCallback<Response>() {

            @Override
            public void onCompleted(Response response) {

                ArrayList<Customer> customers = response.getCustomers();

                AppLogger.log(TAG_PROCESS_NAME, "Fetch from Salesforce Successful. Received "
                        + customers.size() + " customers from Salesforce for upserting");

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                upsertSap(customers);
            }

            @Override
            public void onError(Throwable e) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when fetching customer records from Salesforce. " + e.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

        };

        syncDataService.getCustomers(getFromSalesforceCallback);

    }

    private void upsertSap(ArrayList<Customer> customers) {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        customers = (ArrayList<Customer>) updateSyncFields(customers, false, false);

        DataService.GetCallback<ArrayList<Customer>> upsertCustomersCallback = new DataService.GetCallback<ArrayList<Customer>>() {

            @Override
            public void onCompleted(ArrayList<Customer> customers) {

                if (customers.size() > 0) {
                    AppLogger.log(TAG_PROCESS_NAME, "Upserting on SAP successful");
                }

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                getUnsyncedSalesforceCustomers();

            }

            @Override
            public void onError(Throwable e) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when upserting customer records. " + e.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

        };

        dbService.upsertRecords(customers, upsertCustomersCallback);
    }

    private void getUnsyncedSalesforceCustomers() {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        DataService.GetCallback<ArrayList<Customer>> getUnsyncedCustomersCallback = new DataService.GetCallback<ArrayList<Customer>>() {

            @Override
            public void onCompleted(ArrayList<Customer> customerArrayList) {

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                pushToSalesforce(customerArrayList);
            }

            @Override
            public void onError(Throwable e) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when fetching unsynced customer records from Sap. " + e.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);
            }

        };

        dbService.getUnsynced(getUnsyncedCustomersCallback);

    }

    private void pushToSalesforce(ArrayList<Customer> customers) {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        customers = (ArrayList<Customer>) updateSyncFields(customers, false, false);

        int customersCount = customers.size();

        AppLogger.log(TAG_PROCESS_NAME, "Found " + customersCount + " customers that need to be pushed to Salesforce. " + (customersCount > 0 ? "Attempting to push." : ""));

        if (customersCount > 0) {

            DataService.GetCallback<ArrayList<Customer>> upsertCustomersCallback = new DataService.GetCallback<ArrayList<Customer>>() {

                @Override
                public void onCompleted(ArrayList<Customer> response) {

                    AppLogger.log(TAG_PROCESS_NAME, "Customers sync complete");

                    syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                }

                @Override
                public void onError(Throwable e) {

                    AppLogger.log(TAG_PROCESS_NAME, "An error occurred when upserting customer records. " + e.getMessage());

                    syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                }

            };

            DataService.GetCallback<Response> pushToSalesforceCallback = new DataService.GetCallback<Response>() {

                @Override
                public void onCompleted(Response response) {

                    ArrayList<Customer> customers = response.getCustomers();

                    int customersCount = customers.size();

                    AppLogger.log(TAG_PROCESS_NAME, "Push To Salesforce Successful. Received "
                            + customersCount + " customers from Salesforce for updating");

                    customers = (ArrayList<Customer>) updateSyncFields(customers, false, false);

                    dbService.upsertRecords(customers, upsertCustomersCallback);

                }

                @Override
                public void onError(Throwable e) {

                    AppLogger.log(TAG_PROCESS_NAME, "An error occurred when pushing customer records to Salesforce. " + e.getMessage());

                    syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);
                }
            };

            syncDataService.pushCustomersToSalesforce(Response.setCustomers(customers), pushToSalesforceCallback);

        } else {

            AppLogger.log(TAG_PROCESS_NAME, "Customers sync complete");

            syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);
        }
    }

}
