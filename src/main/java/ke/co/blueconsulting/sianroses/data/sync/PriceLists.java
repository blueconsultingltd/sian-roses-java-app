package ke.co.blueconsulting.sianroses.data.sync;

import ke.co.blueconsulting.sianroses.contract.SyncContract;
import ke.co.blueconsulting.sianroses.data.DataService;
import ke.co.blueconsulting.sianroses.data.impl.SyncDataService;
import ke.co.blueconsulting.sianroses.data.impl.db.PriceListDbService;
import ke.co.blueconsulting.sianroses.model.app.Response;
import ke.co.blueconsulting.sianroses.model.salesforce.PriceList;
import ke.co.blueconsulting.sianroses.util.AppLogger;

import java.util.ArrayList;

import static ke.co.blueconsulting.sianroses.util.UpdateFields.updateSyncFields;

public class PriceLists {

    private final String TAG_PROCESS_NAME = "price_lists";
    private final PriceListDbService dbService;
    private final SyncContract.View syncDashboard;
    private final SyncDataService syncDataService;

    public PriceLists(SyncContract.View syncDashboard, SyncDataService syncDataService) {

        this.syncDashboard = syncDashboard;

        this.syncDataService = syncDataService;

        this.dbService = new PriceListDbService();
    }

    public void sync() {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        DataService.GetCallback<ArrayList<PriceList>> getRecordsWithPullFromSapCheckedTrueCallback = new DataService.GetCallback<ArrayList<PriceList>>() {

            @Override
            public void onCompleted(ArrayList<PriceList> priceLists) {

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                pushToSalesforce(priceLists);

            }

            @Override
            public void onError(Throwable t) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when querying price lists. " + t.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

        };

        dbService.getRecordsWithPullFromSapCheckedTrue(getRecordsWithPullFromSapCheckedTrueCallback);

    }

    private void pushToSalesforce(ArrayList<PriceList> priceLists) {

        final int[] count = {priceLists.size()};

        AppLogger.log(TAG_PROCESS_NAME, "Found " + count[0] + " price lists that need to be pushed to Salesforce. " + (count[0] > 0 ? "Attempting to push." : ""));

        if (count[0] > 0) {

            syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

            DataService.GetCallback<Response> pushToSalesforceCallback = new DataService.GetCallback<Response>() {

                @Override
                public void onCompleted(Response response) {

                    ArrayList<PriceList> priceLists = response.getPriceList();

                    count[0] = priceLists.size();

                    AppLogger.log(TAG_PROCESS_NAME, "Push To Salesforce Successful. Received " + count + " price lists from Salesforce for updating");

                    syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                    upsertSap(priceLists);

                }

                @Override
                public void onError(Throwable t) {

                    AppLogger.log(TAG_PROCESS_NAME, "An error occurred when pushing to records to Salesforce. " + t.getMessage());

                    syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                }

            };

            syncDataService.pushPriceListToSalesforce(Response.setPriceList(priceLists), pushToSalesforceCallback);
        }


    }

    private void upsertSap(ArrayList<PriceList> priceLists) {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        priceLists = (ArrayList<PriceList>) updateSyncFields(priceLists, false, false);

        DataService.GetCallback<ArrayList<PriceList>> upsertPriceListsCallback = new DataService.GetCallback<ArrayList<PriceList>>() {

            @Override
            public void onCompleted(ArrayList<PriceList> returnedPriceLists) {

                AppLogger.log(TAG_PROCESS_NAME, "Price lists sync complete");

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

            @Override
            public void onError(Throwable e) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when upserting price lists. " + e.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

        };

        dbService.upsertRecords(priceLists, upsertPriceListsCallback);
    }

}
