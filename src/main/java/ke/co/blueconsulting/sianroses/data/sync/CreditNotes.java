package ke.co.blueconsulting.sianroses.data.sync;

import ke.co.blueconsulting.sianroses.contract.SyncContract;
import ke.co.blueconsulting.sianroses.data.DataService;
import ke.co.blueconsulting.sianroses.data.impl.SyncDataService;
import ke.co.blueconsulting.sianroses.data.impl.db.CreditNoteDbService;
import ke.co.blueconsulting.sianroses.data.impl.db.CreditNoteItemDbService;
import ke.co.blueconsulting.sianroses.model.app.Response;
import ke.co.blueconsulting.sianroses.model.salesforce.coupled.CreditNote;
import ke.co.blueconsulting.sianroses.model.salesforce.coupled.CreditNoteItem;
import ke.co.blueconsulting.sianroses.util.AppLogger;

import java.util.ArrayList;

import static ke.co.blueconsulting.sianroses.util.UpdateFields.updateSyncFields;

public class CreditNotes {

    private final String TAG_PROCESS_NAME = "credit_notes";
    private final CreditNoteDbService creditNoteDbService;
    private final CreditNoteItemDbService creditNoteItemDbService;
    private final SyncContract.View syncDashboard;
    private final SyncDataService syncDataService;

    public CreditNotes(SyncContract.View syncDashboard, SyncDataService syncDataService) {

        this.syncDashboard = syncDashboard;

        this.syncDataService = syncDataService;

        this.creditNoteDbService = new CreditNoteDbService();

        this.creditNoteItemDbService = new CreditNoteItemDbService();
    }

    public void sync() {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        DataService.GetCallback<Response> getFromSalesforceCallback = new DataService.GetCallback<Response>() {

            @Override
            public void onCompleted(Response response) {

                ArrayList<CreditNote> lists = response.getCreditNotes();

                AppLogger.log(TAG_PROCESS_NAME, "Fetch from Salesforce Successful. Received "
                        + lists.size() + " credit notes from Salesforce for upserting");

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                upsertPackinglistsInSap(lists);

            }

            @Override
            public void onError(Throwable e) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when fetching list records from Salesforce. " + e.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

        };

        syncDataService.getCreditNotes(getFromSalesforceCallback);

    }

    private void upsertPackinglistsInSap(ArrayList<CreditNote> lists) {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        lists = (ArrayList<CreditNote>) updateSyncFields(lists, false, false);

        DataService.GetCallback<ArrayList<CreditNote>> upsertCreditNotesCallback = new DataService.GetCallback<ArrayList<CreditNote>>() {

            @Override
            public void onCompleted(ArrayList<CreditNote> lists) {

                if (lists.size() > 0) {

                    AppLogger.log(TAG_PROCESS_NAME, "Upserting on SAP successful");

                    lists.forEach((creditNote) -> {
                        if (creditNote.getAssociatedCasesPackingListItems() != null) {
                            ArrayList<CreditNoteItem> records = creditNote.getAssociatedCasesPackingListItems().
                                    getCreditNoteItems();
                            upsertCreditNoteItemsInSap(records, creditNote.getAutoId());
                        }
                    });
                }

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);
            }

            @Override
            public void onError(Throwable e) {

                e.printStackTrace();

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when upserting list records. " + e.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

        };

        creditNoteDbService.upsertRecords(lists, upsertCreditNotesCallback);
    }

    private void upsertCreditNoteItemsInSap(ArrayList<CreditNoteItem> records, int autoId) {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        DataService.GetCallback<ArrayList<CreditNoteItem>> upsertRecordsCallback = new DataService.GetCallback<ArrayList<CreditNoteItem>>() {

            @Override
            public void onCompleted(ArrayList<CreditNoteItem> creditNoteItems) {

            }

            @Override
            public void onError(Throwable t) {
                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when upserting packing list items. " + t.getMessage());
            }
        };

        creditNoteItemDbService.upsertRecords(records, autoId, upsertRecordsCallback);

    }

}
