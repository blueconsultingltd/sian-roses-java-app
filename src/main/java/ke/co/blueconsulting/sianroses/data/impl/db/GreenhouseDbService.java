package ke.co.blueconsulting.sianroses.data.impl.db;

import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import ke.co.blueconsulting.sianroses.data.BaseDbDataService;
import ke.co.blueconsulting.sianroses.model.salesforce.Greenhouse;
import ke.co.blueconsulting.sianroses.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class GreenhouseDbService extends BaseDbDataService<Greenhouse> {

    private Exception exception = null;

    @Override
    protected Class<Greenhouse> getDaoServiceClass() {
        return Greenhouse.class;
    }

    public void upsertRecords(ArrayList<Greenhouse> greenhouses, GetCallback<ArrayList<Greenhouse>> callback) {

        ArrayList<Greenhouse> upsertedGreenhouses = new ArrayList<>();

        greenhouses.forEach((greenhouse) -> {

            try {

                boolean recordExists = dao.queryBuilder().where().eq("AUTOID", greenhouse.getAutoId()).countOf() > 0;

                if (recordExists) {

                    UpdateBuilder<Greenhouse, Integer> updateBuilder = dao.updateBuilder();

                    if (!StringUtils.isNullOrEmpty(greenhouse.getSalesforceId())) {
                        updateBuilder.updateColumnValue("SalesForceId", new SelectArg(greenhouse.getSalesforceId()));
                    }

                    if (!StringUtils.isNullOrEmpty(greenhouse.getFarm())) {
                        updateBuilder.updateColumnValue("Farm__c", new SelectArg(greenhouse.getFarm()));
                    }

                    if (!StringUtils.isNullOrEmpty(greenhouse.getFarmCode())) {
                        updateBuilder.updateColumnValue("Farm_Code__c", new SelectArg(greenhouse.getFarmCode()));
                    }

                    if (!StringUtils.isNullOrEmpty(greenhouse.getName())) {
                        updateBuilder.updateColumnValue("Name", new SelectArg(greenhouse.getName()));
                    }

                    if (!StringUtils.isNullOrEmpty(greenhouse.getWarehouseCode())) {
                        updateBuilder.updateColumnValue("Warehouse_Code__c", new SelectArg(greenhouse.getWarehouseCode()));
                    }

                    if (!StringUtils.isNullOrEmpty(greenhouse.getVarietyName())) {
                        updateBuilder.updateColumnValue("U_variety", new SelectArg(greenhouse.getVarietyName()));
                    }

                    updateBuilder.updateColumnValue("Push_to_SAP__c", greenhouse.isPushToSap());

                    updateBuilder.updateColumnValue("Pull_from_SAP__c", greenhouse.isPullFromSap());

                    updateBuilder.where().eq("AUTOID", new SelectArg(greenhouse.getAutoId()));

                    updateBuilder.prepare();

                    updateBuilder.update();

                    QueryBuilder<Greenhouse, Integer> queryBuilder = dao.queryBuilder();

                    Where<Greenhouse, Integer> where = queryBuilder.where();

                    where = where.eq("AUTOID", new SelectArg(greenhouse.getAutoId()));

                    queryBuilder.setWhere(where);

                    List<Greenhouse> insertedProductList = dao.query(queryBuilder.prepare());

                    greenhouse = insertedProductList.get(0);

                } else {
                    dao.createOrUpdate(greenhouse);
                }
                upsertedGreenhouses.add(greenhouse);
            } catch (Exception ex) {
                exception = ex;
            }
        });

        if (exception != null) {
            callback.onError(exception);
        } else {
            callback.onCompleted(upsertedGreenhouses);
        }

    }
}
