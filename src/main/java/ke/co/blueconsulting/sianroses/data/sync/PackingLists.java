package ke.co.blueconsulting.sianroses.data.sync;

import ke.co.blueconsulting.sianroses.contract.SyncContract;
import ke.co.blueconsulting.sianroses.data.DataService;
import ke.co.blueconsulting.sianroses.data.impl.SyncDataService;
import ke.co.blueconsulting.sianroses.data.impl.db.PackingListDbService;
import ke.co.blueconsulting.sianroses.data.impl.db.PackingListItemDbService;
import ke.co.blueconsulting.sianroses.model.app.Response;
import ke.co.blueconsulting.sianroses.model.salesforce.PackingList;
import ke.co.blueconsulting.sianroses.model.salesforce.PackingListItem;
import ke.co.blueconsulting.sianroses.util.AppLogger;

import java.util.ArrayList;

import static ke.co.blueconsulting.sianroses.util.UpdateFields.updateSyncFields;

public class PackingLists {

    private final String TAG_PROCESS_NAME = "packing_lists";
    private final PackingListDbService packingListDbService;
    private final PackingListItemDbService packingListItemDbService;
    private final SyncContract.View syncDashboard;
    private final SyncDataService syncDataService;

    public PackingLists(SyncContract.View syncDashboard, SyncDataService syncDataService) {

        this.syncDashboard = syncDashboard;

        this.syncDataService = syncDataService;

        packingListDbService = new PackingListDbService();

        packingListItemDbService = new PackingListItemDbService();
    }

    public void sync() {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        DataService.GetCallback<Response> getFromSalesforceCallback = new DataService.GetCallback<Response>() {

            @Override
            public void onCompleted(Response response) {

                ArrayList<PackingList> lists = response.getPackingLists();

                AppLogger.log(TAG_PROCESS_NAME, "Fetch from Salesforce Successful. Received "
                        + lists.size() + " lists from Salesforce for upserting");

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                upsertPackinglistsInSap(lists);
            }

            @Override
            public void onError(Throwable e) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when fetching list records from Salesforce. " + e.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

        };

        syncDataService.getPackingLists(getFromSalesforceCallback);

    }

    private void upsertPackinglistsInSap(ArrayList<PackingList> lists) {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        lists = (ArrayList<PackingList>) updateSyncFields(lists, false, false);

        DataService.GetCallback<ArrayList<PackingList>> upsertPackingListsCallback = new DataService.GetCallback<ArrayList<PackingList>>() {

            @Override
            public void onCompleted(ArrayList<PackingList> lists) {

                if (lists.size() > 0) {
                    AppLogger.log(TAG_PROCESS_NAME, "Upserting on SAP successful");

                    lists.forEach((packingList) -> {
                        ArrayList<PackingListItem> records = packingList.getPackingListItems().getRecords();
                        if (records != null) {
                            upsertPackinglistItemsInSap(records, packingList.getAutoId());
                        }
                    });
                }

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                getUnsyncedSalesforcePackingLists();

            }

            @Override
            public void onError(Throwable e) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when upserting list records. " + e.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

        };

        packingListDbService.upsertRecords(lists, upsertPackingListsCallback);
    }

    private void getUnsyncedSalesforcePackingLists() {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        DataService.GetCallback<ArrayList<PackingList>> getUnsyncedPackingListsCallback = new DataService.GetCallback<ArrayList<PackingList>>() {

            @Override
            public void onCompleted(ArrayList<PackingList> listArrayList) {

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                pushToSalesforce(listArrayList);
            }

            @Override
            public void onError(Throwable e) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when fetching unsynced list records from Sap. " + e.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);
            }

        };

        packingListDbService.getUnsynced(getUnsyncedPackingListsCallback);

    }

    private void pushToSalesforce(ArrayList<PackingList> lists) {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        lists = (ArrayList<PackingList>) updateSyncFields(lists, false, false);

        int listsCount = lists.size();

        AppLogger.log(TAG_PROCESS_NAME, "Found " + listsCount + " lists that need to be pushed to Salesforce. " + (listsCount > 0 ? "Attempting to push." : ""));

        if (listsCount > 0) {

            DataService.GetCallback<ArrayList<PackingList>> upsertPackingListsCallback = new DataService.GetCallback<ArrayList<PackingList>>() {

                @Override
                public void onCompleted(ArrayList<PackingList> response) {

                    AppLogger.log(TAG_PROCESS_NAME, "PackingLists sync complete");

                    syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                }

                @Override
                public void onError(Throwable e) {

                    AppLogger.log(TAG_PROCESS_NAME, "An error occurred when upserting list records. " + e.getMessage());

                    syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                }

            };

            DataService.GetCallback<Response> pushToSalesforceCallback = new DataService.GetCallback<Response>() {

                @Override
                public void onCompleted(Response response) {

                    ArrayList<PackingList> lists = response.getPackingLists();

                    int listsCount = lists.size();

                    AppLogger.log(TAG_PROCESS_NAME, "Push To Salesforce Successful. Received "
                            + listsCount + " lists from Salesforce for updating");

                    lists = (ArrayList<PackingList>) updateSyncFields(lists, false, false);

                    packingListDbService.upsertRecords(lists, upsertPackingListsCallback);

                }

                @Override
                public void onError(Throwable e) {

                    AppLogger.log(TAG_PROCESS_NAME, "An error occurred when pushing list records to Salesforce. " + e.getMessage());

                    syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);
                }
            };

            syncDataService.pushPackingListsToSalesforce(Response.setPackingLists(lists), pushToSalesforceCallback);

        } else {

            AppLogger.log(TAG_PROCESS_NAME, "Packing Lists sync complete");

            syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);
        }
    }

    private void upsertPackinglistItemsInSap(ArrayList<PackingListItem> records, int autoId) {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        DataService.GetCallback<ArrayList<PackingListItem>> upsertRecordsCallback = new DataService.GetCallback<ArrayList<PackingListItem>>() {

            @Override
            public void onCompleted(ArrayList<PackingListItem> response) {

            }

            @Override
            public void onError(Throwable t) {
                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when upserting packing list items. " + t.getMessage());
            }
        };

        packingListItemDbService.upsertRecords(records, autoId, upsertRecordsCallback);

    }

}
