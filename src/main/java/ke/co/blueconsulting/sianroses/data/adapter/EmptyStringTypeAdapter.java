package ke.co.blueconsulting.sianroses.data.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import ke.co.blueconsulting.sianroses.util.StringUtils;

import java.io.IOException;

import static ke.co.blueconsulting.sianroses.data.adapter.CurrencyIsoCodeAdapter.getString;

public class EmptyStringTypeAdapter extends TypeAdapter<String> {

    private EmptyStringTypeAdapter() {

    }

    @Override
    @SuppressWarnings("resource")
    public void write(final JsonWriter jsonWriter, final String string)
            throws IOException {
        if (StringUtils.isNullOrEmpty(string) || StringUtils.isBlank(string)) {
            jsonWriter.nullValue();
        } else {
            jsonWriter.value(string);
        }
    }

    @Override
    public String read(JsonReader jsonReader) throws IOException {
        final JsonToken token = jsonReader.peek();
        return getString(jsonReader, token);
    }

}
