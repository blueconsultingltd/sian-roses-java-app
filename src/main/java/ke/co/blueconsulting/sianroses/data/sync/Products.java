package ke.co.blueconsulting.sianroses.data.sync;

import ke.co.blueconsulting.sianroses.contract.SyncContract;
import ke.co.blueconsulting.sianroses.data.DataService;
import ke.co.blueconsulting.sianroses.data.impl.SyncDataService;
import ke.co.blueconsulting.sianroses.data.impl.db.ProductDbService;
import ke.co.blueconsulting.sianroses.model.app.Response;
import ke.co.blueconsulting.sianroses.model.salesforce.Product;
import ke.co.blueconsulting.sianroses.util.AppLogger;

import java.util.ArrayList;

import static ke.co.blueconsulting.sianroses.util.UpdateFields.updateSyncFields;

public class Products {

    private final String TAG_PROCESS_NAME = "products";
    private final ProductDbService dbService;
    private final SyncContract.View syncDashboard;
    private final SyncDataService syncDataService;

    public Products(SyncContract.View syncDashboard, SyncDataService syncDataService) {

        this.syncDashboard = syncDashboard;

        this.syncDataService = syncDataService;

        this.dbService = new ProductDbService();

    }

    public void sync() {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        DataService.GetCallback<ArrayList<Product>> getRecordsWithPullFromSapCheckedTrueCallback = new DataService.GetCallback<ArrayList<Product>>() {

            @Override
            public void onCompleted(ArrayList<Product> products) {

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                pushToSalesforce(products);

            }

            @Override
            public void onError(Throwable t) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when querying products. " + t.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);
            }

        };

        dbService.getRecordsWithPullFromSapCheckedTrue(getRecordsWithPullFromSapCheckedTrueCallback);

    }

    private void pushToSalesforce(ArrayList<Product> products) {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        int productsCount = products.size();

        AppLogger.log(TAG_PROCESS_NAME, "Found " + productsCount + " products that need to be pushed to Salesforce. " + (productsCount > 0 ? "Attempting to push." : ""));

        DataService.GetCallback<Response> pushToSalesforceCallback = new DataService.GetCallback<Response>() {

            @Override
            public void onCompleted(Response response) {

                ArrayList<Product> products = response.getProducts();

                int productsCount = products.size();

                AppLogger.log(TAG_PROCESS_NAME, "Push To Salesforce Successful. " + "Received " + productsCount + " products from Salesforce for updating");

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                upsertSap(products);

            }

            @Override
            public void onError(Throwable t) {

                AppLogger.log(TAG_PROCESS_NAME, "Failed to push products to Salesforce. " + t.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);
            }
        };

        syncDataService.pushProductsToSalesforce(Response.setProducts(products), pushToSalesforceCallback);

    }

    private void upsertSap(ArrayList<Product> products) {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        products = (ArrayList<Product>) updateSyncFields(products, false, false);

        DataService.GetCallback<ArrayList<Product>> upsertProductsCallback = new DataService.GetCallback<ArrayList<Product>>() {

            @Override
            public void onCompleted(ArrayList<Product> returnedProducts) {

                AppLogger.log(TAG_PROCESS_NAME, "Products sync complete");

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

            @Override
            public void onError(Throwable e) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when upserting products. " + e.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

        };

        dbService.upsertRecords(products, upsertProductsCallback);
    }

}
