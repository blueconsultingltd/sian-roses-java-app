package ke.co.blueconsulting.sianroses.data.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CurrencyIsoCodeAdapter extends TypeAdapter<String> {

    private CurrencyIsoCodeAdapter() {
    }

    static String getString(JsonReader jsonReader, JsonToken token) throws IOException {
        switch (token) {
            case NULL:
                return "";
            case STRING:
                return jsonReader.nextString();
            default:
                throw new IllegalStateException("Unexpected token: " + token);
        }
    }

    @Override
    public void write(final JsonWriter jsonWriter, final String string)
            throws IOException {
        String regex = "[a-zA-Z0-9]*";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(string);
        if (string.isEmpty() || matcher.find()) {
            jsonWriter.nullValue();
        } else {
            jsonWriter.value(string);
        }
    }

    @Override
    public String read(JsonReader jsonReader) throws IOException {
        final JsonToken token = jsonReader.peek();
        return getString(jsonReader, token);
    }

}
