package ke.co.blueconsulting.sianroses.data.sync;

import ke.co.blueconsulting.sianroses.contract.SyncContract;
import ke.co.blueconsulting.sianroses.data.DataService;
import ke.co.blueconsulting.sianroses.data.impl.SyncDataService;
import ke.co.blueconsulting.sianroses.data.impl.db.GreenhouseDbService;
import ke.co.blueconsulting.sianroses.model.app.Response;
import ke.co.blueconsulting.sianroses.model.salesforce.Greenhouse;
import ke.co.blueconsulting.sianroses.util.AppLogger;

import java.util.ArrayList;

import static ke.co.blueconsulting.sianroses.util.UpdateFields.updateSyncFields;

public class Greenhouses {

    private final String TAG_PROCESS_NAME = "greenhouses";
    private final SyncDataService syncDataService;
    private final GreenhouseDbService dbService;
    private final SyncContract.View syncDashboard;

    public Greenhouses(SyncContract.View syncDashboard, SyncDataService syncDataService) {

        this.syncDashboard = syncDashboard;

        this.syncDataService = syncDataService;

        this.dbService = new GreenhouseDbService();

    }

    public void sync() {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        DataService.GetCallback<ArrayList<Greenhouse>> getRecordsWithPullFromSapCheckedTrueCallback = new DataService.GetCallback<ArrayList<Greenhouse>>() {

            @Override
            public void onCompleted(ArrayList<Greenhouse> greenhouses) {

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                pushToSaleforce(greenhouses);
            }

            @Override
            public void onError(Throwable t) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when querying greenhouses. " + t.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);
            }

        };

        dbService.getRecordsWithPullFromSapCheckedTrue(getRecordsWithPullFromSapCheckedTrueCallback);

    }

    private void pushToSaleforce(ArrayList<Greenhouse> greenhouses) {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        DataService.GetCallback<Response> pushWarehousesToSalesforceCallback = new DataService.GetCallback<Response>() {

            @Override
            public void onCompleted(Response response) {

                ArrayList<Greenhouse> greenhouses = response.getGreenhouses();

                int warehousesCount = greenhouses.size();

                AppLogger.log(TAG_PROCESS_NAME, "Push To Salesforce Successful. " + "Received " + warehousesCount + " greenhouses from Salesforce for updating");

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                upsertSap(greenhouses);

            }

            @Override
            public void onError(Throwable t) {

                AppLogger.log(TAG_PROCESS_NAME, "Failed to push greenhouses to Salesforce. " + t.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }
        };

        syncDataService.pushWarehousesToSalesforce(Response.setWarehouses(greenhouses), pushWarehousesToSalesforceCallback);

    }

    private void upsertSap(ArrayList<Greenhouse> greenhouses) {


        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        greenhouses = (ArrayList<Greenhouse>) updateSyncFields(greenhouses, false, false);

        DataService.GetCallback<ArrayList<Greenhouse>> upsertRecordsCallback = new DataService.GetCallback<ArrayList<Greenhouse>>() {

            @Override
            public void onCompleted(ArrayList<Greenhouse> response) {

                AppLogger.log(TAG_PROCESS_NAME, "Greenhouses sync is complete");

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

            @Override
            public void onError(Throwable t) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when upserting greenhouses. " + t.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }
        };

        dbService.upsertRecords(greenhouses, upsertRecordsCallback);

    }
}
