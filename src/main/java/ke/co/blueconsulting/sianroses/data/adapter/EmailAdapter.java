package ke.co.blueconsulting.sianroses.data.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ke.co.blueconsulting.sianroses.data.adapter.CurrencyIsoCodeAdapter.getString;

public class EmailAdapter extends TypeAdapter<String> {

    private EmailAdapter() {
    }

    @Override
    public void write(final JsonWriter jsonWriter, final String email)
            throws IOException {
        String regex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        if (!matcher.find()) {
            jsonWriter.nullValue();
        } else {
            jsonWriter.value(email);
        }
    }

    @Override
    public String read(JsonReader jsonReader) throws IOException {
        final JsonToken token = jsonReader.peek();
        return getString(jsonReader, token);
    }

}
