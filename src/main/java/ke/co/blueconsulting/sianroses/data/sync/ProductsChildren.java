package ke.co.blueconsulting.sianroses.data.sync;

import ke.co.blueconsulting.sianroses.contract.SyncContract;
import ke.co.blueconsulting.sianroses.data.DataService;
import ke.co.blueconsulting.sianroses.data.impl.SyncDataService;
import ke.co.blueconsulting.sianroses.data.impl.db.ProductChildDbService;
import ke.co.blueconsulting.sianroses.model.app.Response;
import ke.co.blueconsulting.sianroses.model.salesforce.ProductChild;
import ke.co.blueconsulting.sianroses.util.AppLogger;

import java.util.ArrayList;

import static ke.co.blueconsulting.sianroses.util.UpdateFields.updateSyncFields;

public class ProductsChildren {

    private final String TAG_PROCESS_NAME = "products_children";
    private final ProductChildDbService dbService;
    private final SyncContract.View syncDashboard;
    private final SyncDataService syncDataService;

    public ProductsChildren(SyncContract.View syncDashboard, SyncDataService syncDataService) {

        this.syncDashboard = syncDashboard;

        this.syncDataService = syncDataService;

        this.dbService = new ProductChildDbService();
    }

    public void sync() {


        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        DataService.GetCallback<ArrayList<ProductChild>> getRecordsWithPullFromSapCheckedTrueCallback = new DataService.GetCallback<ArrayList<ProductChild>>() {

            @Override
            public void onCompleted(ArrayList<ProductChild> productsChildren) {

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                pushToSalesforce(productsChildren);
            }

            @Override
            public void onError(Throwable t) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when querying products child. " + t.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);
            }

        };

        dbService.getRecordsWithPullFromSapCheckedTrue(getRecordsWithPullFromSapCheckedTrueCallback);

    }

    private void pushToSalesforce(ArrayList<ProductChild> productsChildren) {


        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);

        int size = productsChildren.size();

        AppLogger.log(TAG_PROCESS_NAME, "Found " + size + " products' children that need to be pushed to Salesforce. " + (size > 0 ? "Attempting to push." : ""));


        DataService.GetCallback<Response> pushProductsChildrenToSalesforceCallback = new DataService.GetCallback<Response>() {

            @Override
            public void onCompleted(Response response) {

                ArrayList<ProductChild> productsChildren = response.getProductsChildren();

                int productsChildrenCount = productsChildren.size();

                AppLogger.log(TAG_PROCESS_NAME, "Push To Salesforce Successful. "
                        + "Received " + productsChildrenCount + " products' children' children from Salesforce for updating");

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

                upsertSap(productsChildren);
            }

            @Override
            public void onError(Throwable t) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when upserting customer records. " + t.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

        };

        syncDataService.pushProductsChildrenToSalesforce(Response.setProductsChildren(productsChildren),
                pushProductsChildrenToSalesforceCallback);
    }

    private void upsertSap(ArrayList<ProductChild> productsChildren) {

        syncDashboard.addToProcessStack(TAG_PROCESS_NAME);


        productsChildren = (ArrayList<ProductChild>) updateSyncFields(productsChildren, false, false);

        DataService.GetCallback<ArrayList<ProductChild>> upsertProductsCallback = new DataService.GetCallback<ArrayList<ProductChild>>() {

            @Override
            public void onCompleted(ArrayList<ProductChild> returnedProducts) {

                AppLogger.log(TAG_PROCESS_NAME, "Products' children sync complete");

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

            @Override
            public void onError(Throwable e) {

                AppLogger.log(TAG_PROCESS_NAME, "An error occurred when upserting customer records. " + e.getMessage());

                syncDashboard.removeFromProcessStack(TAG_PROCESS_NAME);

            }

        };

        dbService.upsertRecords(productsChildren, upsertProductsCallback);
    }
}
