package ke.co.blueconsulting.sianroses.data.impl.db;

import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import ke.co.blueconsulting.sianroses.data.BaseDbDataService;
import ke.co.blueconsulting.sianroses.model.salesforce.DbCreditNoteItem;
import ke.co.blueconsulting.sianroses.model.salesforce.coupled.CreditNoteItem;
import ke.co.blueconsulting.sianroses.model.salesforce.coupled.PackingListInvoice;
import ke.co.blueconsulting.sianroses.model.salesforce.coupled.PackingListItem;
import ke.co.blueconsulting.sianroses.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class CreditNoteItemDbService extends BaseDbDataService<DbCreditNoteItem> {

    private Exception exception = null;

    @Override
    protected Class<DbCreditNoteItem> getDaoServiceClass() {
        return DbCreditNoteItem.class;
    }

    public void upsertRecords(ArrayList<CreditNoteItem> creditNotes, int autoId, GetCallback<ArrayList<CreditNoteItem>> callback) {

        ArrayList<CreditNoteItem> upsertedCreditNotes = new ArrayList<>();

        creditNotes.forEach((creditNoteItem) -> {

            try {

                DbCreditNoteItem dbCreditNote;

                boolean recordExists = dao.queryBuilder().where().eq("SalesForceId", creditNoteItem.getSalesforceId()).countOf() > 0;

                if (recordExists) {

                    UpdateBuilder<DbCreditNoteItem, Integer> updateBuilder = dao.updateBuilder();

                    if (creditNoteItem.getPackingListItem() != null) {
                        updateBuilder.updateColumnValue("Flower_Code__c", new SelectArg(creditNoteItem.getPackingListItem().getFlowerCode()));
                    }

                    if (creditNoteItem.getPackingListItem() != null && !StringUtils.isNullOrEmpty(creditNoteItem.getPackingListItem().getName())) {
                        updateBuilder.updateColumnValue("Name", new SelectArg(creditNoteItem.getPackingListItem().getName()));
                    }

                    if (creditNoteItem.getPackingListItem() != null) {
                        updateBuilder.updateColumnValue("Quantity__c", new SelectArg(creditNoteItem.getPackingListItem().getQuantity()));
                    }

                    if (creditNoteItem.getPackingListItem() != null) {
                        updateBuilder.updateColumnValue("UnitPrice", new SelectArg(creditNoteItem.getPackingListItem().getUnitPrice()));
                    }

                    updateBuilder.updateColumnValue("Push_to_SAP__c", creditNoteItem.isPushToSap());

                    updateBuilder.updateColumnValue("Pull_from_SAP__c", creditNoteItem.isPullFromSap());

                    updateBuilder.where().eq("SalesForceId", new SelectArg(creditNoteItem.getSalesforceId()));

                    updateBuilder.prepare();

                    updateBuilder.update();

                    QueryBuilder<DbCreditNoteItem, Integer> queryBuilder = dao.queryBuilder();

                    Where<DbCreditNoteItem, Integer> where = queryBuilder.where();

                    where = where.eq("SalesForceId", new SelectArg(creditNoteItem.getSalesforceId()));

                    queryBuilder.setWhere(where);

                    List<DbCreditNoteItem> insertedProductList = dao.query(queryBuilder.prepare());

                    dbCreditNote = insertedProductList.get(0);

                } else {
                    dbCreditNote = castMergeToDbCreditNote(creditNoteItem);
                    dao.createOrUpdate(dbCreditNote);
                }

                creditNoteItem = castMergeToCreditNote(creditNoteItem, dbCreditNote);

                upsertedCreditNotes.add(creditNoteItem);

            } catch (Exception ex) {
                exception = ex;
            }
        });

        if (exception != null) {
            exception.printStackTrace();
            callback.onError(exception);
        } else {
            callback.onCompleted(upsertedCreditNotes);
        }

    }

    private DbCreditNoteItem castMergeToDbCreditNote(CreditNoteItem creditNote) {
        DbCreditNoteItem dbCreditNote = new DbCreditNoteItem();
        dbCreditNote.setAutoId(creditNote.getAutoId());
        dbCreditNote.setPullFromSap(creditNote.isPullFromSap());
        dbCreditNote.setPushToSap(creditNote.isPushToSap());
        dbCreditNote.setSalesforceId(creditNote.getSalesforceId());
        if (creditNote.getPackingListItem() != null) {
            dbCreditNote.setFlowerCode(creditNote.getPackingListItem().getFlowerCode());
            dbCreditNote.setName(creditNote.getPackingListItem().getName());
            dbCreditNote.setQuantity(creditNote.getPackingListItem().getQuantity());
            dbCreditNote.setUnitPrice(creditNote.getPackingListItem().getUnitPrice());
        }
        return dbCreditNote;
    }

    private CreditNoteItem castMergeToCreditNote(CreditNoteItem creditNote, DbCreditNoteItem dbCreditNote) {

        CreditNoteItem mergedCreditNote = new CreditNoteItem();

        mergedCreditNote.setAutoId(creditNote.getAutoId() != 0 ? creditNote.getAutoId() : dbCreditNote.getAutoId());

        mergedCreditNote.setPullFromSap(creditNote.isPullFromSap() || dbCreditNote.isPullFromSap());

        mergedCreditNote.setPushToSap(creditNote.isPushToSap() || dbCreditNote.isPushToSap());

        mergedCreditNote.setSalesforceId(creditNote.getSalesforceId() != null ? creditNote.getSalesforceId() : dbCreditNote.getSalesforceId());

        PackingListItem packingListItem = new PackingListItem();

        packingListItem.setUnitPrice(dbCreditNote.getUnitPrice());

        packingListItem.setName(dbCreditNote.getName());

        packingListItem.setQuantity(dbCreditNote.getQuantity());

        packingListItem.setFlowerCode(dbCreditNote.getFlowerCode());

        mergedCreditNote.setPackingListItem(packingListItem);

        PackingListInvoice packingListInvoice = new PackingListInvoice();

        packingListInvoice.setInvoice(dbCreditNote.getInvoiceEntry());

        mergedCreditNote.setPackingListInvoice(packingListInvoice);

        return mergedCreditNote;
    }

}
