package ke.co.blueconsulting.sianroses.data.impl.db;

import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import ke.co.blueconsulting.sianroses.data.BaseDbDataService;
import ke.co.blueconsulting.sianroses.model.salesforce.PackingListItem;
import ke.co.blueconsulting.sianroses.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class PackingListItemDbService extends BaseDbDataService<PackingListItem> {

    private Exception exception = null;

    @Override
    protected Class<PackingListItem> getDaoServiceClass() {
        return PackingListItem.class;
    }

    public void upsertRecords(ArrayList<PackingListItem> packingListItems, int autoIdMst, GetCallback<ArrayList<PackingListItem>> callback) {

        ArrayList<PackingListItem> upsertedPackingListItems = new ArrayList<>();

        packingListItems.forEach((packingListItem) -> {

            try {

                boolean recordExists = dao.queryBuilder().where().eq("SalesForceId", packingListItem.getSalesforceId()).countOf() > 0;

                packingListItem.setAutoIdMst(autoIdMst);

                if (recordExists) {

                    UpdateBuilder<PackingListItem, Integer> updateBuilder = dao.updateBuilder();

                    if (!StringUtils.isNullOrEmpty(packingListItem.getFlowerCode())) {
                        updateBuilder.updateColumnValue("Flower_Code__c", new SelectArg(packingListItem.getFlowerCode()));
                    }

                    if (!StringUtils.isNullOrEmpty(packingListItem.getName())) {
                        updateBuilder.updateColumnValue("Name", new SelectArg(packingListItem.getName()));
                    }

                    if (!StringUtils.isNullOrEmpty(packingListItem.getQuantity())) {
                        updateBuilder.updateColumnValue("Quantity__c", new SelectArg(packingListItem.getQuantity()));
                    }

                    if (!StringUtils.isNullOrEmpty(packingListItem.getUnitPrice())) {
                        updateBuilder.updateColumnValue("UnitPrice", new SelectArg(packingListItem.getUnitPrice()));
                    }

                    updateBuilder.updateColumnValue("Push_to_SAP__c", packingListItem.isPushToSap());

                    updateBuilder.updateColumnValue("Pull_from_SAP__c", packingListItem.isPullFromSap());

                    updateBuilder.where().eq("SalesForceId", new SelectArg(packingListItem.getSalesforceId()));

                    updateBuilder.prepare();

                    updateBuilder.update();

                    QueryBuilder<PackingListItem, Integer> queryBuilder = dao.queryBuilder();

                    Where<PackingListItem, Integer> where = queryBuilder.where();

                    where = where.eq("SalesForceId", new SelectArg(packingListItem.getSalesforceId()));

                    queryBuilder.setWhere(where);

                    List<PackingListItem> insertedProductList = dao.query(queryBuilder.prepare());

                    packingListItem = insertedProductList.get(0);

                } else {
                    dao.createOrUpdate(packingListItem);
                }

                upsertedPackingListItems.add(packingListItem);

            } catch (Exception ex) {
                exception = ex;
            }
        });

        if (exception != null) {
            callback.onError(exception);
        } else {
            callback.onCompleted(upsertedPackingListItems);
        }


    }

    @Override
    public PackingListItem insertRecord(PackingListItem packingList) throws Exception {
        return super.insertRecord(packingList);
    }
}
