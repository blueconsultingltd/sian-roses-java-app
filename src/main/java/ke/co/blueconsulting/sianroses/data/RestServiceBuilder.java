package ke.co.blueconsulting.sianroses.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ke.co.blueconsulting.sianroses.data.deserializer.ErrorsDeserializer;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

import static ke.co.blueconsulting.sianroses.util.Constants.SALESFORCE_API_BASE_URL;
import static ke.co.blueconsulting.sianroses.util.Constants.SALESFORCE_AUTH_BASE_URL;
import static ke.co.blueconsulting.sianroses.util.Constants.Units.*;

public class RestServiceBuilder {

    private static Retrofit.Builder builder;

    private static Retrofit retrofit;

    static {
        switchToApiBaseUrl();
    }

    public static Retrofit retrofit() {
        return retrofit;
    }

    public static void switchToApiBaseUrl() {
        builder = getRetrofitBuilder().baseUrl(SALESFORCE_API_BASE_URL);
        retrofit = builder.build();
    }

    public static void switchToAuthUrl() {
        builder = getRetrofitBuilder().baseUrl(SALESFORCE_AUTH_BASE_URL);
        retrofit = builder.build();
    }

    private static GsonConverterFactory buildGsonConverter() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(ErrorsDeserializer.Parser.class, new ErrorsDeserializer())
                .excludeFieldsWithoutExposeAnnotation()
                .create();
        return GsonConverterFactory.create(gson);
    }

    private static Retrofit.Builder getRetrofitBuilder() {
        return new Retrofit.Builder()
                .addConverterFactory(buildGsonConverter())
                .client(getSafeOkHttpClient());
    }

    private static OkHttpClient.Builder getOkHttpClientBuilder() {
        //add accept data type
        Interceptor acceptHeader = chain -> {
            Request request = chain.request().newBuilder()
                    .addHeader("Accept", "application/json")
                    .addHeader("Content-Type", "application/json;charset=UTF-8")
                    .build();
            return chain.proceed(request);
        };

        //add logging
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder().addInterceptor(acceptHeader)
                .addInterceptor(httpLoggingInterceptor)
                .readTimeout(READ_TIME_OUT_MINUTES, TimeUnit.MINUTES)
                .writeTimeout(WRITE_TIME_OUT_MINUTES, TimeUnit.MINUTES)
                .connectTimeout(CONNECT_TIME_OUT_MINUTES, TimeUnit.MINUTES)
                .retryOnConnectionFailure(true);
    }

    private static OkHttpClient getSafeOkHttpClient() {
        return getOkHttpClientBuilder().build();
    }

    static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}
