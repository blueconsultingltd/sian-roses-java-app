package ke.co.blueconsulting.sianroses.data.impl.db;

import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import ke.co.blueconsulting.sianroses.data.BaseDbDataService;
import ke.co.blueconsulting.sianroses.model.salesforce.Stock;
import ke.co.blueconsulting.sianroses.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class StocksDbService extends BaseDbDataService<Stock> {

    private Exception exception = null;

    @Override
    protected Class<Stock> getDaoServiceClass() {
        return Stock.class;
    }

    public void upsertRecords(ArrayList<Stock> stocks, GetCallback<ArrayList<Stock>> callback) {

        ArrayList<Stock> upsertedStocks = new ArrayList<>();

        stocks.forEach((stock) -> {
            try {
                boolean recordExists = dao.queryBuilder().where().eq("AUTOID", stock.getAutoId()).countOf() > 0;

                if (recordExists) {

                    UpdateBuilder<Stock, Integer> updateBuilder = dao.updateBuilder();

                    if (!StringUtils.isNullOrEmpty(stock.getSalesforceId())) {
                        updateBuilder.updateColumnValue("SalesForceId", new SelectArg(stock.getSalesforceId()));
                    }

                    if (!StringUtils.isNullOrEmpty(stock.getFarm())) {
                        updateBuilder.updateColumnValue("Farm__c", new SelectArg(stock.getFarm()));
                    }

                    if (!StringUtils.isNullOrEmpty(stock.getFarmCode())) {
                        updateBuilder.updateColumnValue("Farm_Code__c", new SelectArg(stock.getFarmCode()));
                    }

                    if (!StringUtils.isNullOrEmpty(stock.getName())) {
                        updateBuilder.updateColumnValue("Name", new SelectArg(stock.getName()));
                    }

                    if (!StringUtils.isNullOrEmpty(stock.getWarehouse())) {
                        updateBuilder.updateColumnValue("Warehouse__c", new SelectArg(stock.getWarehouse()));
                    }

                    if (!StringUtils.isNullOrEmpty(stock.getQuantity())) {
                        updateBuilder.updateColumnValue("Quantity__c", new SelectArg(stock.getQuantity()));
                    }

                    updateBuilder.updateColumnValue("Push_to_SAP__c", stock.isPushToSap());

                    updateBuilder.updateColumnValue("Pull_from_SAP__c", stock.isPullFromSap());

                    updateBuilder.where().eq("AUTOID", new SelectArg(stock.getAutoId()));

                    updateBuilder.prepare();

                    updateBuilder.update();

                    QueryBuilder<Stock, Integer> queryBuilder = dao.queryBuilder();

                    Where<Stock, Integer> where = queryBuilder.where();

                    where = where.eq("AUTOID", new SelectArg(stock.getAutoId()));

                    queryBuilder.setWhere(where);

                    List<Stock> insertedStockList = dao.query(queryBuilder.prepare());

                    stock = insertedStockList.get(0);

                } else {
                    dao.createOrUpdate(stock);
                }
                upsertedStocks.add(stock);
            } catch (Exception ex) {
                exception = ex;
            }
        });

        if (exception != null) {
            callback.onError(exception);
        } else {
            callback.onCompleted(upsertedStocks);
        }


    }

    public void getRecordsWithPullFromSapCheckedTrue(GetCallback<ArrayList<Stock>> callback, long limit, long startId) {
        try {
            QueryBuilder<Stock, Integer> queryBuilder = dao.queryBuilder();
            Where<Stock, Integer> where = queryBuilder.where();
            queryBuilder.setWhere(where.or(where.isNull("Pull_from_SAP__c"),
                    where.eq("Pull_from_SAP__c", true)).and().gt("AUTOID", startId));
            queryBuilder.orderBy("AUTOID", true);
            if (limit != 0) {
                queryBuilder.limit(limit);
            }
            if (callback != null) {
                callback.onCompleted((ArrayList<Stock>) dao.query(queryBuilder.prepare()));
            }
        } catch (Exception e) {
            if (callback != null) {
                callback.onError(e);
            }
        }

    }

}
