package ke.co.blueconsulting.sianroses.data.impl.db;

import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import ke.co.blueconsulting.sianroses.data.BaseDbDataService;
import ke.co.blueconsulting.sianroses.model.salesforce.DbCreditNote;
import ke.co.blueconsulting.sianroses.model.salesforce.coupled.CreditNote;
import ke.co.blueconsulting.sianroses.model.salesforce.coupled.PackingListInvoice;
import ke.co.blueconsulting.sianroses.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class CreditNoteDbService extends BaseDbDataService<DbCreditNote> {

    private Exception exception = null;

    @Override
    protected Class<DbCreditNote> getDaoServiceClass() {
        return DbCreditNote.class;
    }

    public void upsertRecords(ArrayList<CreditNote> creditNotes, GetCallback<ArrayList<CreditNote>> callback) {

        ArrayList<CreditNote> upsertedCreditNotes = new ArrayList<>();

        creditNotes.forEach((creditNote) -> {

            try {

                DbCreditNote dbCreditNote;

                boolean recordExists = dao.queryBuilder().where().eq("SalesForceId", creditNote.getSalesforceId()).countOf() > 0;

                if (recordExists) {

                    UpdateBuilder<DbCreditNote, Integer> updateBuilder = dao.updateBuilder();

                    if (!StringUtils.isNullOrEmpty(creditNote.getErpId())) {
                        updateBuilder.updateColumnValue("ERP_ID__c", new SelectArg(creditNote.getErpId()));
                    }

                    if (creditNote.getPackingListInvoice() != null) {
                        updateBuilder.updateColumnValue("AccountID", new SelectArg(creditNote.getPackingListInvoice().getAccountId()));
                    }

                    updateBuilder.updateColumnValue("Posting_Date__c", new SelectArg(creditNote.getPostingDate()));

                    if (!StringUtils.isNullOrEmpty(creditNote.getDueDate())) {
                        updateBuilder.updateColumnValue("Due_Date__c", new SelectArg(creditNote.getDueDate()));
                    }

                    if (!StringUtils.isNullOrEmpty(creditNote.getpONumber())) {
                        updateBuilder.updateColumnValue("PO_Number__c", new SelectArg(creditNote.getpONumber()));
                    }

                    if (!StringUtils.isNullOrEmpty(creditNote.getInvoiceNumber())) {
                        updateBuilder.updateColumnValue("Invoice_Number__c", new SelectArg(creditNote.getInvoiceNumber()));
                    }

                    if (!StringUtils.isNullOrEmpty(creditNote.getInvoiceEntry())) {
                        updateBuilder.updateColumnValue("Invoice_Entry__c", new SelectArg(creditNote.getInvoiceEntry()));
                    }

                    if (!StringUtils.isNullOrEmpty(creditNote.getFarm())) {
                        updateBuilder.updateColumnValue("Farm_c", new SelectArg(creditNote.getFarm()));
                    }

                    if (!StringUtils.isNullOrEmpty(creditNote.getReason())) {
                        updateBuilder.updateColumnValue("Reason", new SelectArg(creditNote.getReason()));
                    }

                    updateBuilder.updateColumnValue("Push_to_SAP__c", creditNote.isPushToSap());

                    updateBuilder.updateColumnValue("Pull_from_SAP__c", creditNote.isPullFromSap());

                    updateBuilder.where().eq("SalesForceId", new SelectArg(creditNote.getSalesforceId()));

                    updateBuilder.prepare();

                    updateBuilder.update();

                    QueryBuilder<DbCreditNote, Integer> queryBuilder = dao.queryBuilder();

                    Where<DbCreditNote, Integer> where = queryBuilder.where();

                    where = where.eq("SalesForceId", new SelectArg(creditNote.getSalesforceId()));

                    queryBuilder.setWhere(where);

                    List<DbCreditNote> insertedProductList = dao.query(queryBuilder.prepare());

                    dbCreditNote = insertedProductList.get(0);

                } else {
                    dbCreditNote = castMergeToDbCreditNote(creditNote);
                    dao.createOrUpdate(dbCreditNote);
                }

                creditNote = castMergeToCreditNote(creditNote, dbCreditNote);

                upsertedCreditNotes.add(creditNote);

            } catch (Exception ex) {
                exception = ex;
            }
        });

        if (exception != null) {
            callback.onError(exception);
        } else {
            callback.onCompleted(upsertedCreditNotes);
        }

    }

    private DbCreditNote castMergeToDbCreditNote(CreditNote creditNote) {
        DbCreditNote dbCreditNote = new DbCreditNote();
        dbCreditNote.setAutoId(creditNote.getAutoId());
        dbCreditNote.setPullFromSap(creditNote.isPullFromSap());
        dbCreditNote.setPushToSap(creditNote.isPushToSap());
        dbCreditNote.setSalesforceId(creditNote.getSalesforceId());
        dbCreditNote.setErpId(creditNote.getErpId());
        if (creditNote.getPackingListInvoice() != null) {
            dbCreditNote.setAccountId(creditNote.getPackingListInvoice().getAccountId());
        }
        dbCreditNote.setPostingDate(creditNote.getPostingDate());
        dbCreditNote.setDueDate(creditNote.getDueDate());
        dbCreditNote.setpONumber(creditNote.getpONumber());
        dbCreditNote.setInvoiceNumber(creditNote.getInvoiceNumber());
        dbCreditNote.setInvoiceEntry(creditNote.getInvoiceEntry());
        dbCreditNote.setFarm(creditNote.getFarm());
        dbCreditNote.setReason(creditNote.getReason());
        return dbCreditNote;
    }

    private CreditNote castMergeToCreditNote(CreditNote creditNote, DbCreditNote dbCreditNote) {

        CreditNote mergedCreditNote = new CreditNote();

        mergedCreditNote.setAutoId(creditNote.getAutoId() != 0 ? creditNote.getAutoId() : dbCreditNote.getAutoId());

        mergedCreditNote.setPullFromSap(creditNote.isPullFromSap() || dbCreditNote.isPullFromSap());

        mergedCreditNote.setPushToSap(creditNote.isPushToSap() || dbCreditNote.isPushToSap());

        mergedCreditNote.setSalesforceId(creditNote.getSalesforceId() != null ? creditNote.getSalesforceId() : dbCreditNote.getSalesforceId());

        mergedCreditNote.setErpId(creditNote.getErpId() != null ? creditNote.getErpId() : dbCreditNote.getErpId());

        mergedCreditNote.setPostingDate(creditNote.getPostingDate() != null ? creditNote.getPostingDate() : dbCreditNote.getPostingDate());

        mergedCreditNote.setDueDate(creditNote.getDueDate() != null ? creditNote.getDueDate() : dbCreditNote.getDueDate());

        mergedCreditNote.setpONumber(creditNote.getpONumber() != null ? creditNote.getpONumber() : dbCreditNote.getpONumber());

        mergedCreditNote.setInvoiceNumber(creditNote.getInvoiceNumber() != null ? creditNote.getInvoiceNumber() : dbCreditNote.getInvoiceNumber());

        mergedCreditNote.setInvoiceEntry(creditNote.getInvoiceEntry() != null ? creditNote.getInvoiceEntry() : dbCreditNote.getInvoiceEntry());

        mergedCreditNote.setFarm(creditNote.getFarm() != null ? creditNote.getFarm() : dbCreditNote.getFarm());

        mergedCreditNote.setReason(creditNote.getReason() != null ? creditNote.getReason() : dbCreditNote.getReason());

        if (!StringUtils.isNullOrEmpty(dbCreditNote.getAccountId())) {
            mergedCreditNote.setPackingListInvoice(new PackingListInvoice(dbCreditNote.getAccountId()));
        }

        mergedCreditNote.setAssociatedCasesPackingListItems(creditNote.getAssociatedCasesPackingListItems());

        return mergedCreditNote;
    }

}
