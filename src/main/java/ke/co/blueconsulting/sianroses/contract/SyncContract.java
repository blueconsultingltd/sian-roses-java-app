package ke.co.blueconsulting.sianroses.contract;

import ke.co.blueconsulting.sianroses.model.app.AppAuthCredentials;

public class SyncContract {

    public interface View {

        void showErrorMessage(String message);

        void showErrorMessage(String title, String message);

        void updateUiFields(AppAuthCredentials appAuthCredentialsData);

        void showSuccessMessage(String message);

        void showToolTip(String message, int code);

        void addToProcessStack(String processName);

        void removeFromProcessStack(String processName);

        void clearProcessStack();
    }

    public interface Presenter {

        void testDbConnection(String serverAddress, String serverPort, String databaseName,
                              String databaseUsername, String databasePassword, String syncPeriod, String syncPeriodUnit)
                throws Exception;

        void performSync() throws Exception;

        void saveSAPAuthCredentials(String serverAddress, String serverPort, String databaseName, String databaseUsername,
                                    String databasePassword, String syncPeriod, String syncPeriodUnit) throws Exception;

        void getCredentials() throws Exception;

        void testSalesforceAuth(String salesforceClientId, String salesforceClientSecret, String salesforceUsername,
                                String salesforcePassword, String salesforceSecurityToken);

        void saveSalesforceAuth(String salesforceClientId, String salesforceClientSecret, String salesforceUsername,
                                String salesforcePassword, String salesforceSecurityToken) throws Exception;

        void cancelSync();
    }
}
