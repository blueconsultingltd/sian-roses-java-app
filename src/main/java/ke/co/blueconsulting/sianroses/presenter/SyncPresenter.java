package ke.co.blueconsulting.sianroses.presenter;

import ke.co.blueconsulting.sianroses.SyncDashboard;
import ke.co.blueconsulting.sianroses.contract.SyncContract;
import ke.co.blueconsulting.sianroses.data.BaseDbDataService;
import ke.co.blueconsulting.sianroses.data.DataService;
import ke.co.blueconsulting.sianroses.data.RestServiceBuilder;
import ke.co.blueconsulting.sianroses.data.impl.AuthDataService;
import ke.co.blueconsulting.sianroses.data.impl.SyncDataService;
import ke.co.blueconsulting.sianroses.data.impl.db.AuthCredentialsDbService;
import ke.co.blueconsulting.sianroses.model.app.AppAuthCredentials;
import ke.co.blueconsulting.sianroses.model.app.SalesforceAuthCredentials;
import ke.co.blueconsulting.sianroses.util.StringUtils;

import java.util.Timer;

import static ke.co.blueconsulting.sianroses.util.Constants.UIStringKeys.MESSAGE_LOGIN_FAILED;
import static ke.co.blueconsulting.sianroses.util.StringUtils.getString;

public class SyncPresenter implements SyncContract.Presenter {

    private final String SALESFORCE_AUTH_PROCESS_NAME = "THE_FORCE_AUTH";
    private final String DB_AUTH_PROCESS_NAME = "DB_AUTH";
    private Thread connectThread;
    private SyncContract.View syncDashboard;
    private AuthCredentialsDbService authCredentialsDbService;
    private SyncDataService syncDataService;
    private Timer timer;


    public SyncPresenter(SyncDashboard syncDashboard) throws Exception {
        this.syncDashboard = syncDashboard;
        this.authCredentialsDbService = new AuthCredentialsDbService();
        this.syncDataService = new SyncDataService();
    }

    private boolean hasAccessToken() {
        AppAuthCredentials credentials = authCredentialsDbService.getAppAuthCredentials();
        return !StringUtils.isNullOrEmpty(credentials.getSalesforceAccessToken())
                && !StringUtils.isBlank(credentials.getSalesforceAccessToken());
    }

    public boolean hasCredentials() {
        AppAuthCredentials credentials = authCredentialsDbService.getAppAuthCredentials();
        return !StringUtils.isNullOrEmpty(credentials.getServerAddress())
                && !StringUtils.isBlank(credentials.getServerAddress())
                && !StringUtils.isNullOrEmpty(String.valueOf(credentials.getServerPort()))
                && !StringUtils.isBlank(String.valueOf(credentials.getServerPort()))
                && !StringUtils.isNullOrEmpty(credentials.getDatabaseName())
                && !StringUtils.isBlank(credentials.getDatabaseName())
                && !StringUtils.isNullOrEmpty(credentials.getDatabaseUsername())
                && !StringUtils.isBlank(credentials.getDatabaseUsername())
                && !StringUtils.isNullOrEmpty(credentials.getDatabasePassword())
                && !StringUtils.isBlank(credentials.getDatabasePassword())
                && !StringUtils.isNullOrEmpty(String.valueOf(credentials.getSyncPeriod()))
                && !StringUtils.isBlank(String.valueOf(credentials.getSyncPeriod()))
                && !StringUtils.isNullOrEmpty(credentials.getSyncPeriodUnit())
                && !StringUtils.isBlank(credentials.getSyncPeriodUnit())
                && !StringUtils.isNullOrEmpty(credentials.getSalesforceClientId())
                && !StringUtils.isBlank(credentials.getSalesforceClientId())
                && !StringUtils.isNullOrEmpty(credentials.getSalesforceClientSecret())
                && !StringUtils.isBlank(credentials.getSalesforceClientSecret())
                && !StringUtils.isNullOrEmpty(credentials.getSalesforceUsername())
                && !StringUtils.isBlank(credentials.getSalesforceUsername())
                && !StringUtils.isNullOrEmpty(credentials.getSalesforcePassword())
                && !StringUtils.isBlank(credentials.getSalesforcePassword())
                && !StringUtils.isNullOrEmpty(credentials.getSalesforceSecurityToken())
                && !StringUtils.isBlank(credentials.getSalesforceSecurityToken());
    }

    private AuthDataService getAuthService() {
        RestServiceBuilder.switchToAuthUrl();
        return new AuthDataService();
    }

    @Override
    public void getCredentials() {

        syncDashboard.addToProcessStack(DB_AUTH_PROCESS_NAME);

        try {
            AppAuthCredentials connectionData = authCredentialsDbService.getAppAuthCredentials();
            if (connectionData != null) {
                syncDashboard.updateUiFields(connectionData);
            }
        } finally {
            syncDashboard.removeFromProcessStack(DB_AUTH_PROCESS_NAME);
        }
    }

    @Override
    public void testDbConnection(String serverAddress, String serverPort, String databaseName,
                                 String databaseUsername, String databasePassword, String syncPeriod,
                                 String syncPeriodUnit) {

        syncDashboard.addToProcessStack(DB_AUTH_PROCESS_NAME);

        final boolean[] connectionSuccessful = {false};

        connectThread = new Thread(() -> {
            try {
                if (BaseDbDataService.testSAPConnection(serverAddress, serverPort, databaseName, databaseUsername,
                        databasePassword)) {
                    connectionSuccessful[0] = true;
                }
            } catch (Exception e) {
                syncDashboard.showErrorMessage(e.getMessage());
            } finally {
                syncDashboard.removeFromProcessStack(DB_AUTH_PROCESS_NAME);
                if (connectionSuccessful[0]) {
                    saveSAPAuthCredentials(serverAddress, serverPort, databaseName, databaseUsername, databasePassword, syncPeriod,
                            syncPeriodUnit);
                    syncDashboard.showSuccessMessage("SAP connection successful. Credentials Successfully Stored.");
                }
                try {
                    connectThread.join();
                } catch (Exception ignored) {
                }
            }
        });
        connectThread.start();
    }

    /**
     * Saves the user database connections in the sqlite database
     */
    @Override
    public void saveSAPAuthCredentials(String serverAddress, String serverPort, String databaseName,
                                       String databaseUsername, String databasePassword, String syncPeriod,
                                       String syncPeriodUnit) {
        saveSAPAuthCredentials(false, serverAddress, serverPort, databaseName, databaseUsername,
                databasePassword, syncPeriod, syncPeriodUnit);
    }

    public void saveSAPAuthCredentials(boolean showalert, String serverAddress, String serverPort, String databaseName,
                                       String databaseUsername, String databasePassword, String syncPeriod,
                                       String syncPeriodUnit) {
        syncDashboard.addToProcessStack(DB_AUTH_PROCESS_NAME);
        try {
            AppAuthCredentials appAuthCredentials = authCredentialsDbService.getAppAuthCredentials();
            appAuthCredentials.setServerAddress(serverAddress);
            appAuthCredentials.setServerPort(Integer.parseInt(serverPort));
            appAuthCredentials.setDatabaseName(databaseName);
            appAuthCredentials.setDatabaseUsername(databaseUsername);
            appAuthCredentials.setDatabasePassword(databasePassword);
            appAuthCredentials.setSyncPeriod(Integer.parseInt(syncPeriod));
            appAuthCredentials.setSyncPeriodUnit(syncPeriodUnit);
            authCredentialsDbService.save(appAuthCredentials);
            if (showalert) {
                syncDashboard.showSuccessMessage("Saved");
            }
        } catch (Exception e) {
            syncDashboard.showErrorMessage("Failed to store SAP Credentials. " + e.getMessage());
        } finally {
            syncDashboard.removeFromProcessStack(DB_AUTH_PROCESS_NAME);
        }
    }

    @Override
    public void testSalesforceAuth(String salesforceClientId, String salesforceClientSecret,
                                   String salesforceUsername, String salesforcePassword,
                                   String salesforceSecurityToken) {

        syncDashboard.addToProcessStack(SALESFORCE_AUTH_PROCESS_NAME);

        DataService.GetCallback<SalesforceAuthCredentials> authCallback
                = new DataService.GetCallback<SalesforceAuthCredentials>() {
            @Override
            public void onCompleted(SalesforceAuthCredentials response) {
                saveSalesforceSessionCredentials(response);
            }

            @Override
            public void onError(Throwable t) {
                syncDashboard.removeFromProcessStack(SALESFORCE_AUTH_PROCESS_NAME);
                syncDashboard.showErrorMessage(getString(MESSAGE_LOGIN_FAILED), t.getMessage());
            }

        };

        connectThread = new Thread(() -> {
            try {
                getAuthService().authenticate(salesforceClientId, salesforceClientSecret, salesforceUsername,
                        salesforcePassword, salesforceSecurityToken, authCallback);
            } catch (Exception e) {
                syncDashboard.removeFromProcessStack(SALESFORCE_AUTH_PROCESS_NAME);
                syncDashboard.showErrorMessage(getString(MESSAGE_LOGIN_FAILED), e.getMessage());
            } finally {
                try {
                    connectThread.join();
                } catch (Exception ignored) {
                }
            }
        });
        connectThread.start();
    }

    /**
     * Saves the Salesforce authentication details in the sqlite database
     */
    @Override
    public void saveSalesforceAuth(String salesforceClientId, String salesforceClientSecret,
                                   String salesforceUsername, String salesforcePassword,
                                   String salesforceSecurityToken) {
        syncDashboard.addToProcessStack(SALESFORCE_AUTH_PROCESS_NAME);
        try {
            AppAuthCredentials appAuthCredentials = authCredentialsDbService.getAppAuthCredentials();
            appAuthCredentials.setSalesforceClientId(salesforceClientId);
            appAuthCredentials.setSalesforceClientSecret(salesforceClientSecret);
            appAuthCredentials.setSalesforceUsername(salesforceUsername);
            appAuthCredentials.setSalesforcePassword(salesforcePassword);
            appAuthCredentials.setSalesforceSecurityToken(salesforceSecurityToken);
            authCredentialsDbService.save(appAuthCredentials);
            syncDashboard.showSuccessMessage("Saved");
        } catch (Exception e) {
            syncDashboard.showErrorMessage("Failed to store Salesforce Credentials. " + e.getMessage());
        } finally {
            syncDashboard.removeFromProcessStack(SALESFORCE_AUTH_PROCESS_NAME);
        }
    }

    @Override
    public void cancelSync() {
        syncDataService.cancelTask();
        timer.cancel();
        timer.purge();
        syncDashboard.clearProcessStack();
    }

    private void saveSalesforceSessionCredentials(SalesforceAuthCredentials salesforceAuthCredentials) {

        syncDashboard.addToProcessStack(SALESFORCE_AUTH_PROCESS_NAME);

        AppAuthCredentials appAuthCredentials = authCredentialsDbService.getAppAuthCredentials();
        appAuthCredentials.setSalesforceAccessToken(salesforceAuthCredentials.getAccessToken());
        appAuthCredentials.setInstanceUrl(salesforceAuthCredentials.getInstanceUrl());
        appAuthCredentials.setSalesForceId(salesforceAuthCredentials.getId());
        appAuthCredentials.setTokenType(salesforceAuthCredentials.getTokenType());
        appAuthCredentials.setIssuedAt(salesforceAuthCredentials.getIssuedAt());
        appAuthCredentials.setSignature(salesforceAuthCredentials.getSignature());
        try {
            authCredentialsDbService.save(appAuthCredentials);
            syncDashboard.showSuccessMessage("Salesforce authentication successful. Credentials Successfully Stored");
        } catch (Exception e) {
            syncDashboard.showErrorMessage("Failed to store Salesforce credentials. " + e.getMessage());
        } finally {
            syncDashboard.removeFromProcessStack(SALESFORCE_AUTH_PROCESS_NAME);
        }
    }

    @Override
    public void performSync() {
        if (hasAccessToken()) {
            AppAuthCredentials appAuthCredentials = authCredentialsDbService.getAppAuthCredentials();
            int duration = appAuthCredentials.getSyncPeriod();
            int syncTimeWaitSeconds = 0;
            switch (appAuthCredentials.getSyncPeriodUnit()) {
                case "Minute(s)":
                    syncTimeWaitSeconds = duration * 60 * 1000;
                    break;
                case "Hour(s)":
                    syncTimeWaitSeconds = duration * 60 * 60 * 1000;
                    break;
                case "Day(s)":
                    syncTimeWaitSeconds = duration * 60 * 60 * 24 * 1000;
                    break;
                case "Week(s)":
                    syncTimeWaitSeconds = duration * 60 * 60 * 24 * 7 * 1000;
                    break;
            }
            this.timer = new Timer(true);
            timer.schedule(new SyncTask(syncDashboard, syncDataService), 0, syncTimeWaitSeconds);
        } else {
            syncDashboard.showErrorMessage("Cannot get access token from Salesforce. No Login Credentials Found");
        }
    }
}
