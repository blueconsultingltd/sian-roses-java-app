package ke.co.blueconsulting.sianroses.presenter;

import ke.co.blueconsulting.sianroses.contract.SyncContract;
import ke.co.blueconsulting.sianroses.data.RestServiceBuilder;
import ke.co.blueconsulting.sianroses.data.impl.SyncDataService;
import ke.co.blueconsulting.sianroses.data.sync.*;

import java.util.TimerTask;

public class SyncTask extends TimerTask {

    private final SyncDataService syncDataService;
    private final SyncContract.View syncDashboard;

    SyncTask(SyncContract.View syncDashboard, SyncDataService syncDataService) {
        this.syncDashboard = syncDashboard;

        this.syncDataService = syncDataService;
    }

    public void run() {

        RestServiceBuilder.switchToApiBaseUrl();

        new Customers(syncDashboard, syncDataService).sync();

        new CustomerContacts(syncDashboard, syncDataService).sync();

        new Products(syncDashboard, syncDataService).sync();

        new ProductsChildren(syncDashboard, syncDataService).sync();

        new PriceLists(syncDashboard, syncDataService).sync();

        new Greenhouses(syncDashboard, syncDataService).sync();

        new PackingLists(syncDashboard, syncDataService).sync();

        new Stocks(syncDashboard, syncDataService).sync();

        new CreditNotes(syncDashboard, syncDataService).sync();

    }
}
