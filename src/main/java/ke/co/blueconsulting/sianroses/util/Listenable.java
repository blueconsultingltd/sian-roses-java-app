package ke.co.blueconsulting.sianroses.util;

public interface Listenable<K> {

    void setListener(ListListener<K> listener);

    interface ListListener<K> {

        void beforePut(K value);

        void afterPut(K value);

        void beforeRemove(Object key);

        void afterRemove(Object key);
    }
}