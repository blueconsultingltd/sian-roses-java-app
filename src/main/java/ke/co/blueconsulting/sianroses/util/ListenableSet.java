package ke.co.blueconsulting.sianroses.util;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ListenableSet<K> implements Listenable<K>, Set<K> {

    private Set<K> items = new HashSet<>();
    private ListListener<K> listener;

    @Override
    public int size() {
        return items.size();
    }

    @Override
    public boolean isEmpty() {
        return items.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return items.contains(o);
    }

    @NotNull
    @Override
    public Iterator<K> iterator() {
        return items.iterator();
    }

    @NotNull
    @Override
    public Object[] toArray() {
        return items.toArray();
    }

    @NotNull
    @Override
    public <T> T[] toArray(@NotNull T[] a) {
        return items.toArray(a);
    }

    @Override
    public boolean add(K value) {
        if (listener != null) {
            listener.beforePut(value);
        }
        boolean status = items.add(value);
        if (listener != null) {
            listener.afterPut(value);
        }
        return status;
    }

    @Override
    public boolean remove(Object index) {
        if (listener != null) {
            listener.beforeRemove(index);
        }
        boolean status = items.remove(index);
        if (listener != null) {
            listener.afterRemove(index);
        }
        return status;
    }

    @Override
    public boolean containsAll(@NotNull Collection<?> c) {
        return items.containsAll(c);
    }

    @Override
    public boolean addAll(@NotNull Collection<? extends K> c) {
        return items.addAll(c);
    }

    @Override
    public boolean retainAll(@NotNull Collection<?> c) {
        return items.retainAll(c);
    }

    @Override
    public boolean removeAll(@NotNull Collection<?> c) {
        return items.removeAll(c);
    }

    @Override
    public void clear() {
        items.clear();
    }

    @Override
    public void setListener(ListListener<K> listener) {
        this.listener = listener;
    }
}
