package ke.co.blueconsulting.sianroses.util;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static ke.co.blueconsulting.sianroses.util.Constants.APP_LOG_FILE_NAME;
import static ke.co.blueconsulting.sianroses.util.Constants.DataTypeKeys.*;
import static ke.co.blueconsulting.sianroses.util.Constants.getDirectoryPathName;

public class AppLogger {

    private static String logFile;
    private static boolean xmlFileIsFine = false;
    private static Document document = null;
    private static Node appLog, infoMessagesNode, warningMessagesNode, errorMessagesNode;

    static {
        logFile = getDirectoryPathName() + APP_LOG_FILE_NAME;
        if (logFileExists()) {
            parseLogFile();
        }
    }

    /**
     * Get a node containing logs by it's name
     *
     * @param tagName the name of the node
     * @param nodes   list of nodes containing the node
     * @return selected node of null if it doesn't exist
     */
    private static Node getNodeByName(String tagName, NodeList nodes) {
        for (int x = 0; x < nodes.getLength(); x++) {
            Node node = nodes.item(x);
            if (node.getNodeName().equalsIgnoreCase(tagName)) {
                return node;
            }
        }
        return null;
    }

    /**
     * Checks if a log lockFile exists. If it does not it creates a logfile
     *
     * @return true if a log lockFile exists, false if it does not exist
     */
    private static boolean logFileExists() {
        File directory = new File(getDirectoryPathName());
        if (!directory.exists()) {
            directory.mkdirs();
        }
        File file = new File(logFile);
        if (!file.exists()) {
            return createErrorLogFile();
        }
        return true;
    }

    /**
     * Creates an error log lockFile
     *
     * @return true if successful false if not successful
     */
    private static boolean createErrorLogFile() {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docFactory.newDocumentBuilder();
            Document document = docBuilder.newDocument();
            Element rootElement = document.createElement(ROOT_ELEMENT_NODE_NAME);
            document.appendChild(rootElement);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File(logFile));
            transformer.transform(source, result);
            return true;
        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Reads the error log lockFile and initializes nodes into memory for appending
     */
    private static void parseLogFile() {
        try {
            DOMParser parser = new DOMParser();
            parser.parse(logFile);
            document = parser.getDocument();
            NodeList xmlRoot = document.getChildNodes();
            appLog = getNodeByName(ROOT_ELEMENT_NODE_NAME, xmlRoot);
            infoMessagesNode = getNodeByName(INFO_MESSAGES_NODE_NAME, appLog.getChildNodes());
            warningMessagesNode = getNodeByName(WARNING_MESSAGES_NODE_NAME, appLog.getChildNodes());
            errorMessagesNode = getNodeByName(ERROR_MESSAGES_NODE_NAME, appLog.getChildNodes());
            xmlFileIsFine = true;
        } catch (SAXException | IOException e) {
            if (e instanceof SAXParseException) {
                File file = new File(logFile);
                file.delete();
            } else {
                e.printStackTrace();
            }
        }
    }

    /**
     * Adds a message under the info nodes in the xml
     *
     * @param message message being logged
     */
    public static void logInfo(String message) {
        if (xmlFileIsFine) {
            if (infoMessagesNode == null) {
                appLog.appendChild(document.createElement(INFO_MESSAGES_NODE_NAME));
            }
            createElement(message, INFO_MESSAGES_NODE_NAME, appLog.getChildNodes());
        }
    }

    /**
     * Adds a message under the warning nodes in the xml
     *
     * @param message message being logged
     */
    public static void logWarning(String message) {
        if (xmlFileIsFine) {
            if (warningMessagesNode == null) {
                appLog.appendChild(document.createElement(WARNING_MESSAGES_NODE_NAME));
            }
            createElement(message, WARNING_MESSAGES_NODE_NAME, appLog.getChildNodes());
        }
    }

    /**
     * Adds a message under the error nodes in the xml
     *
     * @param message message being logged
     */
    public static void logError(String message) {
        if (xmlFileIsFine) {
            if (errorMessagesNode == null) {
                appLog.appendChild(document.createElement(ERROR_MESSAGES_NODE_NAME));
            }
            createElement(message, ERROR_MESSAGES_NODE_NAME, appLog.getChildNodes());
        }
    }

    /**
     * Creates an element for an object log
     *
     * @param message   The message being logged
     * @param nodeName  the node under which a message should occur
     * @param nodesList the nodes being added
     */
    private static void createElement(String message, String nodeName, NodeList nodesList) {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy h:mm:ss a");
        Element element = document.createElement(MESSAGE_TAG_NAME);
        element.appendChild(document.createTextNode(format.format(date) + ": " + message));
        Node node = getNodeByName(nodeName, nodesList);
        if (node != null) {
            node.appendChild(element);
        }
        writeNode();
    }

    /**
     * Writes nodes into the xml lockFile
     */
    private static void writeNode() {
        try {
            DOMSource source = new DOMSource(document);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(logFile);
            transformer.transform(source, result);
        } catch (TransformerException e) {
            File file = new File(logFile);
            file.delete();
            writeNode();
        }
    }

    /**
     * Adds an object log into the log lockFile
     *
     * @param objectName name of the object experiencing the error
     * @param message    message being logged
     */
    public static void log(String objectName, String message) {
        if ((StringUtils.isNullOrEmpty(objectName) || StringUtils.isBlank(objectName))
                || (StringUtils.isNullOrEmpty(message) || StringUtils.isBlank(message))) {
            return;
        }
        if (xmlFileIsFine) {
            Node pendingMessagesNode = getNodeByName(objectName, appLog.getChildNodes());
            if (pendingMessagesNode == null) {
                appLog.appendChild(document.createElement(objectName));
            }
            createElement(message, objectName, appLog.getChildNodes());
        }
    }

    /**
     * Gets the messages logged in the app
     *
     * @return Map containing list of errors
     */
    public static Map<String, ArrayList<String>> getMessages() {

        Map<String, ArrayList<String>> messages = new HashMap<>();

        NodeList nodes = appLog.getChildNodes();

        for (int index = 0; index < nodes.getLength(); index++) {
            Node node = nodes.item(index);
            if (!node.getNodeName().startsWith("#")) {
                NodeList childNodes = node.getChildNodes();
                ArrayList<String> texts = new ArrayList<>();
                for (int counter = 0; counter < childNodes.getLength(); counter++) {
                    Node childNode = childNodes.item(counter);
                    if (!childNode.getNodeName().startsWith("#")) {
                        texts.add(childNode.getTextContent());
                    }
                }
                messages.put(node.getNodeName(), texts);
            }
        }
        return messages;
    }
}
