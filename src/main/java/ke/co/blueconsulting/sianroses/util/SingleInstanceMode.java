package ke.co.blueconsulting.sianroses.util;

import java.io.File;
import java.io.IOException;

import static ke.co.blueconsulting.sianroses.util.Constants.getDirectoryPathName;

public class SingleInstanceMode {

    private static File lockFile = null;

    public SingleInstanceMode() {
        File directory = new File(getDirectoryPathName());
        if (!directory.exists()) {
            directory.mkdirs();
        }
        String lockFileName = ".lock";
        lockFile = new File(getDirectoryPathName() + File.separator + lockFileName);
    }

    public boolean lockFileExists() {
        /*
        TODO add a feature to delete the lock file in case the app was shut down prematurely
         */
        return lockFile != null && lockFile.exists();
    }

    public void createLockFile() {
        if (lockFile != null && !lockFile.exists()) {
            try {
                lockFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void removeLockFile() {
        if (lockFile != null) {
            lockFile.delete();
        }
    }
}
