package ke.co.blueconsulting.sianroses;

import ke.co.blueconsulting.sianroses.dialog.MessageDialog;
import ke.co.blueconsulting.sianroses.util.AppLogger;
import ke.co.blueconsulting.sianroses.util.SingleInstanceMode;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.Objects;

import static ke.co.blueconsulting.sianroses.util.Constants.UIStringKeys.*;
import static ke.co.blueconsulting.sianroses.util.StringUtils.breakLongString;
import static ke.co.blueconsulting.sianroses.util.StringUtils.getString;

public class SianRosesApp {

    private static SyncDashboard syncDashboard;
    private static TrayIcon trayIcon;
    private static boolean systemTrayIsSupported = false;
    private static SingleInstanceMode singleInstanceMode;

    /**
     * The start point of the application
     *
     * @param args any arguments passed to the app
     */
    public static void main(String[] args) {

        singleInstanceMode = new SingleInstanceMode();

        if (!singleInstanceMode.lockFileExists()) {

            //System.setProperty(LocalLog.LOCAL_LOG_LEVEL_PROPERTY, "ERROR");
            //System.setProperty(LocalLog.LOCAL_LOG_FILE_PROPERTY, AppLogger.getDirectoryName() + File.separator + SQL_LOG_FILE_NAME);

            try {
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Nimbus".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(SianRosesApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }

            EventQueue.invokeLater(() -> {
                try {
                    drawSystemTrayIcon();
                    syncDashboard = new SyncDashboard();
                } catch (Exception e) {
                    AppLogger.logWarning("An error occurred while trying to start the application. " + e.getMessage());
                }
            });
        } else {
            MessageDialog.showMessageDialog(null, "Another instance of the application is running. Check the system tray icons.", getString(MESSAGE_APPLICATION_RUNNING), JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private static void drawSystemTrayIcon() {

        systemTrayIsSupported = SystemTray.isSupported();

        if (!SystemTray.isSupported()) {
            AppLogger.logWarning("SystemTray is not supported");
            return;
        }

        trayIcon = new TrayIcon(Objects.requireNonNull(createIcon("sian_roses_icon.jpg", "Sian Roses App Tray Icon")).getImage());
        trayIcon.setImageAutoSize(true);
        trayIcon.setToolTip(getString(LABEL_APP_NAME));

        MenuItem bringToFrontMenuItem = new MenuItem("Show The Dashboard");
        MenuItem showLogsMenuItem = new MenuItem("View Log");
        MenuItem aboutMenuItem = new MenuItem("About");
        MenuItem exitMenuItem = new MenuItem("Exit");

        PopupMenu popup = new PopupMenu();
        popup.add(bringToFrontMenuItem);
        popup.addSeparator();
        popup.add(showLogsMenuItem);
        popup.addSeparator();
        popup.add(aboutMenuItem);
        popup.addSeparator();
        popup.add(exitMenuItem);

        trayIcon.setPopupMenu(popup);

        SystemTray systemTray = SystemTray.getSystemTray();
        try {
            systemTray.add(trayIcon);
        } catch (AWTException e) {
            AppLogger.logWarning("TrayIcon could not be added.");
            return;
        }

        trayIcon.addActionListener(actionEvent -> SyncDashboard.bringToFront());

        bringToFrontMenuItem.addActionListener(actionEvent -> SyncDashboard.bringToFront());

        aboutMenuItem.addActionListener(actionEvent -> MessageDialog.showMessageDialog(SyncDashboard.dashboardJFrame, breakLongString("Sian Roses Salesforce Integration App.\n\nv 1.0\n\nPowered by Blue Consulting"), getString(TITLE_ABOUT), JOptionPane.INFORMATION_MESSAGE, getBlueConsultingIcon()));

        showLogsMenuItem.addActionListener(actionEvent -> syncDashboard.switchToLogsTab());

        exitMenuItem.addActionListener(actionEvent -> {
            int dialogResult = JOptionPane.showConfirmDialog(SyncDashboard.dashboardJFrame,
                    "Are you sure you want to exit the application?", "Confirm", JOptionPane.YES_NO_OPTION);
            if (dialogResult == JOptionPane.YES_OPTION) {
                systemTray.remove(trayIcon);
                singleInstanceMode.removeLockFile();
                System.exit(0);
            }
        });
        singleInstanceMode.createLockFile();
    }

    static void displayErrorMessage(String message) {
        if (systemTrayIsSupported) {
            trayIcon.displayMessage(getString(LABEL_APP_NAME), message, TrayIcon.MessageType.ERROR);
        }
    }

    static void displayWarningMessage(String message) {
        if (systemTrayIsSupported) {
            trayIcon.displayMessage(getString(LABEL_APP_NAME), message, TrayIcon.MessageType.WARNING);
        }
    }

    static void displayInfoMessage(String message) {
        if (systemTrayIsSupported) {
            trayIcon.displayMessage(getString(LABEL_APP_NAME), message, TrayIcon.MessageType.INFO);
        }
    }

    static void displayOrdinaryMessage(String message) {
        if (systemTrayIsSupported) {
            trayIcon.displayMessage(getString(LABEL_APP_NAME), message, TrayIcon.MessageType.NONE);
        }
    }

    private static ImageIcon createIcon(String fileName, String description) {
        URL imageURL = SianRosesApp.class.getResource("/" + fileName);
        if (imageURL == null) {
            AppLogger.logWarning("Resource not found: " + fileName);
            return null;
        } else {
            return (new ImageIcon(imageURL, description));
        }
    }

    private static ImageIcon getBlueConsultingIcon() {
        return createIcon("blue_consulting_logo.png", "Blue Consulting Icon");
    }

    static ImageIcon getSianRosesIcon() {
        return createIcon("sian_roses_icon.jpg", "Sian Roses App Tray Icon");
    }

}
