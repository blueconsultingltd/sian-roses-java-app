package ke.co.blueconsulting.sianroses;

import ke.co.blueconsulting.sianroses.contract.SyncContract;
import ke.co.blueconsulting.sianroses.dialog.MessageDialog;
import ke.co.blueconsulting.sianroses.model.app.AppAuthCredentials;
import ke.co.blueconsulting.sianroses.presenter.SyncPresenter;
import ke.co.blueconsulting.sianroses.util.*;

import javax.swing.*;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.util.*;
import java.util.List;

import static ke.co.blueconsulting.sianroses.util.Constants.*;
import static ke.co.blueconsulting.sianroses.util.Constants.UIStringKeys.*;
import static ke.co.blueconsulting.sianroses.util.StringUtils.getString;

public class SyncDashboard implements SyncContract.View {

    static JFrame dashboardJFrame;
    private final SyncPresenter syncPresenter;
    private final String[] syncPeriodUnits;
    private JButton testConnectionButton, saveConnectionButton, syncButton;
    private JProgressBar statusProgressBar;
    private String tabOnView = DATABASE_SERVER_CONFIGURATION_TAB;
    private JTextField serverAddressTextField, serverPortTextField, databaseNameTextField, databaseUsernameTextField,
            syncPeriodTextField, salesforceClientIdTextField, salesforceClientSecretTextField,
            salesforceUsernameTextField, salesforceSecurityTokenTextField;
    private JPasswordField databasePasswordTextField, salesforcePasswordTextField;
    private JComboBox<String> syncPeriodUnitComboBox;
    private JTabbedPane tabsContainer;
    private ListenableSet<String> runningProcesses;
    private JTextArea errorLogTextView;
    private boolean syncInProcess = false;


    SyncDashboard() throws Exception {

        this.syncPeriodUnits = getString(SYNC_PERIOD_UNITS).split(",");
        this.syncPresenter = new SyncPresenter(this);
        this.runningProcesses = new ListenableSet<>();
        this.runningProcesses.setListener(new Listenable.ListListener<String>() {

            @Override
            public void beforePut(String value) {
                if (!runningProcesses.isEmpty()) {
                    setIsBusy(true);
                    enableCancelButton(true);
                } else {
                    setIsBusy(false);
                    enableCancelButton(false);
                }
            }

            @Override
            public void afterPut(String value) {
                if (!runningProcesses.isEmpty()) {
                    setIsBusy(true);
                    enableCancelButton(true);
                } else {
                    setIsBusy(false);
                    enableCancelButton(false);
                }
                updateErrorLogview();
            }

            @Override
            public void beforeRemove(Object key) {
                if (!runningProcesses.isEmpty()) {
                    setIsBusy(true);
                    enableCancelButton(true);
                } else {
                    setIsBusy(false);
                    enableCancelButton(false);
                }
            }

            @Override
            public void afterRemove(Object key) {
                if (!runningProcesses.isEmpty()) {
                    setIsBusy(true);
                    enableCancelButton(true);
                } else {
                    setIsBusy(false);
                    enableCancelButton(false);
                }
                updateErrorLogview();
            }

            private void enableCancelButton(boolean enable) {
                syncInProcess = enable;
                if (enable) {
                    syncButton.setText(getString(STOP_SYNC));
                } else {
                    syncButton.setText(getString(START_SYNC));
                }
            }

            private void updateErrorLogview() {

                if (errorLogTextView != null) {

                    Map<String, ArrayList<String>> messages = AppLogger.getMessages();

                    String spacer = "-----------------------------------------------------------------------------";

                    StringBuilder msgStr = new StringBuilder();

                    messages.keySet().forEach((key) -> {
                        msgStr.append("\n").append(key).append("\n").append(spacer).append("\n");
                        messages.get(key).forEach((message) -> msgStr.append(message).append("\n\n"));
                    });

                    errorLogTextView.setText(msgStr.toString());
                }

                System.out.println("================================");
                System.out.println(runningProcesses.size());
                System.out.println("================================");
                Console.logToJson(runningProcesses);
            }
        });
        initViews();
    }

    static void bringToFront() {
        dashboardJFrame.setVisible(true);
        dashboardJFrame.setFocusable(true);
        dashboardJFrame.requestFocus();
    }

    void switchToLogsTab() {
        bringToFront();
        tabsContainer.setSelectedIndex(2);
    }

    private void initViews() {
        dashboardJFrame = new JFrame();
        dashboardJFrame.setResizable(false);
        dashboardJFrame.setTitle(getString(LABEL_APP_NAME));
        dashboardJFrame.setBounds(100, 100, 600, 410);
        dashboardJFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        dashboardJFrame.setIconImage(SianRosesApp.getSianRosesIcon().getImage());
        dashboardJFrame.getContentPane().setLayout(null);
        dashboardJFrame.setLocationRelativeTo(null);
        dashboardJFrame.getContentPane().setLayout(null);

        tabsContainer = new JTabbedPane();
        tabsContainer.setBounds(0, 0, 598, 324);
        tabsContainer.setAlignmentY(Component.BOTTOM_ALIGNMENT);
        tabsContainer.setAlignmentX(Component.RIGHT_ALIGNMENT);

        tabsContainer.addTab(getString(TAB_DATABASE_SERVER_CONFIG), null, makeDbServerConfigPanel());

        tabsContainer.addTab(getString(TAB_SALESFORCE_CONFIG), null, makeSalesforceConfigPanel());

        tabsContainer.addTab(getString(TAB_APP_LOGVIEW), null, makeAppLogViewPanel());

        tabsContainer.addChangeListener(e -> updateTabButtons(tabsContainer.getSelectedIndex()));

        dashboardJFrame.getContentPane().add(tabsContainer);

        statusProgressBar = new JProgressBar();
        statusProgressBar.setBounds(12, 324, 574, 24);
        statusProgressBar.setIndeterminate(true);
        statusProgressBar.setVisible(false);
        dashboardJFrame.getContentPane().add(statusProgressBar);

        testConnectionButton = new JButton(getString(BTN_TEST_DB_CONNECTION));
        testConnectionButton.setBounds(12, 350, 183, 23);
        testConnectionButton.addActionListener(event -> {
            try {
                if (tabOnView.equals(DATABASE_SERVER_CONFIGURATION_TAB) && serverConfigFieldsAreValid()) {
                    syncPresenter.testDbConnection(serverAddressTextField.getText(), serverPortTextField.getText(),
                            databaseNameTextField.getText(), databaseUsernameTextField.getText(),
                            String.valueOf(databasePasswordTextField.getPassword()), syncPeriodTextField.getText(),
                            Objects.requireNonNull(syncPeriodUnitComboBox.getSelectedItem()).toString());
                } else if (tabOnView.equals(SALESFORCE_CONFIGURATION_TAB) && salesforceConfigFieldsAreValid()) {
                    syncPresenter.testSalesforceAuth(salesforceClientIdTextField.getText(),
                            salesforceClientSecretTextField.getText(), salesforceUsernameTextField.getText(),
                            String.valueOf(salesforcePasswordTextField.getPassword()), salesforceSecurityTokenTextField.getText());
                }
            } catch (Exception e) {
                AppLogger.logError("Some error occured. " + e.getMessage());
                showErrorMessage(getString(MESSAGE_ERROR_OCCURRED) + e.getMessage());
            }
        });

        dashboardJFrame.getContentPane().add(testConnectionButton);

        saveConnectionButton = new JButton(getString(BTN_SAVE));
        saveConnectionButton.setBounds(207, 350, 183, 23);
        saveConnectionButton.addActionListener(event -> {
            try {
                if (tabOnView.equals(DATABASE_SERVER_CONFIGURATION_TAB)) {
                    if (serverConfigFieldsAreValid()) {
                        syncPresenter.saveSAPAuthCredentials(true, serverAddressTextField.getText(), serverPortTextField.getText(),
                                databaseNameTextField.getText(), databaseUsernameTextField.getText(), String.valueOf(databasePasswordTextField.getPassword()),
                                syncPeriodTextField.getText(), Objects.requireNonNull(syncPeriodUnitComboBox.getSelectedItem()).toString());
                    }
                } else {
                    if (salesforceConfigFieldsAreValid()) {
                        syncPresenter.saveSalesforceAuth(salesforceClientIdTextField.getText(),
                                salesforceClientSecretTextField.getText(), salesforceUsernameTextField.getText(),
                                String.valueOf(salesforcePasswordTextField.getPassword()), salesforceSecurityTokenTextField.getText());
                    }
                }

            } catch (Exception e) {
                showErrorMessage(getString(MESSAGE_ERROR_OCCURRED) + e.getMessage());
            }
        });

        dashboardJFrame.getContentPane().add(saveConnectionButton);

        syncButton = new JButton(getString(START_SYNC));
        syncButton.setBounds(403, 350, 183, 23);
        dashboardJFrame.getContentPane().add(syncButton);

        syncButton.addActionListener(event -> {
            try {
                if (syncInProcess) {
                    syncPresenter.cancelSync();
                } else {
                    syncPresenter.performSync();
                }
            } catch (Exception e) {
                showErrorMessage(getString(MESSAGE_FATAL_ERROR) + e.getMessage());
            }
        });

        dashboardJFrame.setVisible(true);

        try {
            syncPresenter.getCredentials();
        } catch (Exception e) {
            showErrorMessage(getString(MESSAGE_FATAL_ERROR) + e.getMessage());
        }

    }

    private void updateTabButtons(int selectedIndex) {
        testConnectionButton.setVisible(true);
        saveConnectionButton.setVisible(true);
        switch (selectedIndex) {
            case 0:
                tabOnView = DATABASE_SERVER_CONFIGURATION_TAB;
                testConnectionButton.setText(getString(BTN_TEST_DB_CONNECTION));
                break;
            case 1:
                tabOnView = SALESFORCE_CONFIGURATION_TAB;
                testConnectionButton.setText(getString(BTN_TEST_SALESFORCE_AUTH));
                break;
            default:
                tabOnView = LOG_VIEW_TAB;
                testConnectionButton.setVisible(false);
                saveConnectionButton.setVisible(false);
                break;
        }
    }

    /**
     * Build the MSSQL Server Settings Panel UI
     *
     * @return JPanel
     */
    private JComponent makeDbServerConfigPanel() {
        JPanel jPanel = new JPanel();
        jPanel.setBorder(null);
        jPanel.setLayout(null);
        JLabel lblServerAddress = new JLabel(getString(LABEL_SERVER_ADDRESS));
        lblServerAddress.setBounds(12, 10, 559, 15);
        jPanel.add(lblServerAddress);

        serverAddressTextField = new JTextField();
        serverAddressTextField.setBounds(10, 30, 559, 24);
        jPanel.add(serverAddressTextField);
        serverAddressTextField.setColumns(10);

        JLabel lblServerPort = new JLabel(getString(LABEL_SERVER_PORT));
        lblServerPort.setBounds(12, 55, 559, 15);
        jPanel.add(lblServerPort);

        serverPortTextField = new JTextField();
        serverPortTextField.setBounds(10, 75, 559, 24);
        serverPortTextField.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                String errMsg = getString(MESSAGE_PORT_OUT_OF_RANGE);
                boolean isOkay = false;
                JTextField jTextField = (JTextField) input;
                try {
                    int value = Integer.parseInt(jTextField.getText());
                    if (value < 1 || value > 65535) {
                        throw new Exception(errMsg);
                    }
                    isOkay = true;
                } catch (Exception ex) {
                    showErrorMessage(getString(MESSAGE_VALIDATION_ERROR), errMsg);
                }
                return isOkay;
            }
        });
        jPanel.add(serverPortTextField);
        serverPortTextField.setColumns(10);

        JLabel lblDatabaseName = new JLabel(getString(LABEL_DATABASE_NAME));
        lblDatabaseName.setBounds(12, 100, 559, 15);
        jPanel.add(lblDatabaseName);

        databaseNameTextField = new JTextField();
        databaseNameTextField.setColumns(10);
        databaseNameTextField.setBounds(10, 124, 559, 24);
        jPanel.add(databaseNameTextField);

        JLabel lblDatabaseUsername = new JLabel(getString(LABEL_DATABASE_USERNAME));
        lblDatabaseUsername.setBounds(12, 150, 559, 15);
        jPanel.add(lblDatabaseUsername);

        databaseUsernameTextField = new JTextField();
        databaseUsernameTextField.setColumns(10);
        databaseUsernameTextField.setBounds(10, 170, 559, 24);
        jPanel.add(databaseUsernameTextField);

        JLabel lblDatabasePassword = new JLabel(getString(LABEL_DATABASE_PASSWORD));
        lblDatabasePassword.setBounds(12, 204, 559, 15);
        jPanel.add(lblDatabasePassword);

        databasePasswordTextField = new JPasswordField();
        databasePasswordTextField.setBounds(10, 224, 559, 24);
        jPanel.add(databasePasswordTextField);

        JLabel lblSyncPeriod = new JLabel(getString(LABEL_SYNC_PERIOD));
        lblSyncPeriod.setBounds(12, 250, 559, 15);
        jPanel.add(lblSyncPeriod);

        syncPeriodTextField = new JTextField();
        syncPeriodTextField.setBounds(10, 270, 107, 24);
        syncPeriodTextField.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                String errMsg = getString(MESSAGE_SYNC_UNIT_OUT_OF_RANGE);
                boolean isOkay = false;
                JTextField jTextField = (JTextField) input;
                try {
                    int value = Integer.parseInt(jTextField.getText());
                    if (value < 1 || value > 55) {
                        throw new Exception(errMsg);
                    }
                    isOkay = true;
                } catch (Exception ex) {
                    showErrorMessage(getString(MESSAGE_VALIDATION_ERROR), errMsg);
                }
                return isOkay;
            }
        });
        jPanel.add(syncPeriodTextField);

        syncPeriodUnitComboBox = new JComboBox<>();
        syncPeriodUnitComboBox.setModel(new DefaultComboBoxModel<>(syncPeriodUnits));
        syncPeriodUnitComboBox.setBounds(125, 270, 98, 24);
        jPanel.add(syncPeriodUnitComboBox);

        return jPanel;
    }

    /**
     * Build the Salesforce Config Panel UI
     *
     * @return JPanel
     */
    private JComponent makeSalesforceConfigPanel() {
        JPanel jPanel = new JPanel();
        jPanel.setBorder(null);
        jPanel.setLayout(null);

        JLabel clientId = new JLabel(getString(LABEL_SALESFORCE_CLIENT_ID));
        clientId.setBounds(12, 10, 559, 15);
        jPanel.add(clientId);

        salesforceClientIdTextField = new JTextField();
        salesforceClientIdTextField.setColumns(10);
        salesforceClientIdTextField.setBounds(10, 30, 559, 24);
        jPanel.add(salesforceClientIdTextField);

        JLabel lblClientSecret = new JLabel(getString(LABEL_SALESFORCE_CLIENT_SECRET));
        lblClientSecret.setBounds(12, 55, 559, 15);
        jPanel.add(lblClientSecret);

        salesforceClientSecretTextField = new JTextField();
        salesforceClientSecretTextField.setColumns(10);
        salesforceClientSecretTextField.setBounds(10, 75, 559, 24);
        jPanel.add(salesforceClientSecretTextField);

        JLabel lblUsername = new JLabel(getString(LABEL_SALESFORCE_USERNAME));
        lblUsername.setBounds(12, 100, 559, 15);
        jPanel.add(lblUsername);

        salesforceUsernameTextField = new JTextField();
        salesforceUsernameTextField.setColumns(10);
        salesforceUsernameTextField.setBounds(10, 124, 559, 24);
        jPanel.add(salesforceUsernameTextField);

        JLabel lblSalesforcePassword = new JLabel(getString(LABEL_SALESFORCE_PASSWORD));
        lblSalesforcePassword.setBounds(12, 150, 559, 15);
        jPanel.add(lblSalesforcePassword);

        salesforcePasswordTextField = new JPasswordField();
        salesforcePasswordTextField.setColumns(10);
        salesforcePasswordTextField.setBounds(10, 170, 559, 24);
        jPanel.add(salesforcePasswordTextField);

        JLabel lblSecurityToken = new JLabel(getString(LABEL_SALESFORCE_SECURITY_TOKEN));
        lblSecurityToken.setBounds(12, 200, 559, 15);
        jPanel.add(lblSecurityToken);

        salesforceSecurityTokenTextField = new JTextField();
        salesforceSecurityTokenTextField.setColumns(10);
        salesforceSecurityTokenTextField.setBounds(10, 224, 559, 24);
        jPanel.add(salesforceSecurityTokenTextField);

        return jPanel;
    }

    /**
     * @return JPanel
     */
    private JComponent makeAppLogViewPanel() {
        JPanel jPanel = new JPanel();
        jPanel.setBorder(null);
        jPanel.setLayout(null);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setBounds(12, 12, 569, 269);
        jPanel.add(scrollPane);

        errorLogTextView = new JTextArea();
        errorLogTextView.setLineWrap(true);
        errorLogTextView.setWrapStyleWord(true);
        errorLogTextView.setEditable(false);

        DefaultCaret caret = (DefaultCaret) errorLogTextView.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

        scrollPane.setViewportView(errorLogTextView);

        return jPanel;
    }

    @Override
    public void updateUiFields(AppAuthCredentials appAuthCredentials) {
        serverAddressTextField.setText(appAuthCredentials.getServerAddress());
        if (appAuthCredentials.getServerPort() != 0) {
            serverPortTextField.setText(String.valueOf(appAuthCredentials.getServerPort()));
        }
        databaseNameTextField.setText(appAuthCredentials.getDatabaseName());
        databaseUsernameTextField.setText(appAuthCredentials.getDatabaseUsername());
        databasePasswordTextField.setText(appAuthCredentials.getDatabasePassword());
        syncPeriodTextField.setText(String.valueOf(appAuthCredentials.getSyncPeriod()));
        syncPeriodUnitComboBox.setSelectedItem(appAuthCredentials.getSyncPeriodUnit());
        salesforceClientIdTextField.setText(appAuthCredentials.getSalesforceClientId());
        salesforceClientSecretTextField.setText(appAuthCredentials.getSalesforceClientSecret());
        salesforceUsernameTextField.setText(appAuthCredentials.getSalesforceUsername());
        salesforcePasswordTextField.setText(appAuthCredentials.getSalesforcePassword());
        salesforceSecurityTokenTextField.setText(appAuthCredentials.getSalesforceSecurityToken());
    }

    private void setIsBusy(boolean isBusy) {
        List<Container> containerList = Arrays.asList(serverAddressTextField, serverPortTextField, databaseNameTextField,
                databaseUsernameTextField, databasePasswordTextField, syncPeriodTextField, syncPeriodUnitComboBox,
                salesforceClientIdTextField, salesforceClientSecretTextField, salesforceUsernameTextField,
                salesforcePasswordTextField, salesforceSecurityTokenTextField, testConnectionButton,
                saveConnectionButton);
        enableContainers(!isBusy, containerList);
        syncButton.setEnabled(syncPresenter.hasCredentials());
        statusProgressBar.setVisible(isBusy);
    }

    private void enableContainers(boolean enabled, List<Container> containerList) {
        containerList.forEach((container) -> container.setEnabled(enabled));
    }

    @Override
    public void showSuccessMessage(String message) {
        message = StringUtils.breakLongString(message);
        MessageDialog.showMessageDialog(dashboardJFrame, message, getString(MESSAGE), JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    public void showErrorMessage(String title, String message) {
        message = StringUtils.breakLongString(message);
        MessageDialog.showMessageDialog(dashboardJFrame, message, title, JOptionPane.ERROR_MESSAGE);
    }

    @Override
    public void showErrorMessage(String message) {
        message = StringUtils.breakLongString(message);
        MessageDialog.showMessageDialog(dashboardJFrame, message, getString(MESSAGE_ERROR_OCCURRED), JOptionPane.ERROR_MESSAGE);
    }

    private boolean serverConfigFieldsAreValid() {
        boolean isValid = true;
        List<String> messages = new ArrayList<>();

        String serverAddress = serverAddressTextField.getText().trim();
        String port = serverPortTextField.getText().trim();
        String dbName = databaseNameTextField.getText().trim();
        String dbUserName = databaseUsernameTextField.getText().trim();
        String dbPassword = String.valueOf(databasePasswordTextField.getPassword()).trim();
        String syncPeriod = syncPeriodTextField.getText().trim();
        String syncPeriodUnit = "";
        if (syncPeriodUnitComboBox.getSelectedItem() != null) {
            syncPeriodUnit = syncPeriodUnitComboBox.getSelectedItem().toString().trim();
        }

        if (StringUtils.isBlank(serverAddress) || StringUtils.isNullOrEmpty(serverAddress)) {
            messages.add(getString(MESSAGE_SERVER_ADDRESS_REQUIRED));
            isValid = false;
        } else if (StringUtils.isBlank(port) || StringUtils.isNullOrEmpty(port)) {
            messages.add(getString(MESSAGE_SERVER_PORT_REQUIRED));
            isValid = false;
        }

        if (StringUtils.isBlank(dbName) || StringUtils.isNullOrEmpty(dbName)) {
            messages.add(getString(MESSAGE_DATABASE_NAME_REQUIRED));
            isValid = false;
        }

        if (StringUtils.isBlank(dbUserName) || StringUtils.isNullOrEmpty(dbUserName)) {
            messages.add(getString(MESSAGE_DATABASE_USERNAME_REQUIRED));
            isValid = false;
        }

        if (StringUtils.isBlank(dbPassword) || StringUtils.isNullOrEmpty(dbPassword)) {
            messages.add(getString(MESSAGE_DATABASE_PASSWORD_REQUIRED));
            isValid = false;
        }

        if (StringUtils.isBlank(syncPeriod) || StringUtils.isNullOrEmpty(syncPeriod)) {
            messages.add(getString(MESSAGE_SYNC_PERIOD_REQUIRED));
            isValid = false;
        }

        if (StringUtils.isBlank(syncPeriodUnit) || StringUtils.isNullOrEmpty(syncPeriodUnit)
                || !Arrays.asList(syncPeriodUnits).contains(syncPeriodUnit)) {
            messages.add(getString(MESSAGE_SELECT_PROVIDED_TIME));
            isValid = false;
        }

        StringBuilder msgString = new StringBuilder();
        messages.forEach((message) -> msgString.append(message).append("\n"));
        if (!isValid) {
            showErrorMessage(getString(MESSAGE_VALIDATION_ERROR), /*getString(MESSAGE_CORRECT_ERRORS) + "\n" + */ msgString.toString());
        }
        return isValid;
    }

    /**
     * Check if the Salesforce Configuration UI fields have been filled as
     * required
     *
     * @return boolean True if fields are properly filled in False if a field
     * has not been properly filled
     */
    private boolean salesforceConfigFieldsAreValid() {
        boolean isValid = true;
        List<String> messages = new ArrayList<>();

        String salesforceClientId = salesforceClientIdTextField.getText().trim();
        String salesforceClientSecret = salesforceClientSecretTextField.getText().trim();
        String salesforceUsername = salesforceUsernameTextField.getText().trim();
        String salesforcePassword = String.valueOf(salesforcePasswordTextField.getPassword()).trim();
        String salesforceSecurityToken = salesforceSecurityTokenTextField.getText();

        if (StringUtils.isBlank(salesforceClientId) || StringUtils.isNullOrEmpty(salesforceClientId)) {
            messages.add(getString(MESSAGE_SALESFORCE_CLIENT_ID_REQUIRED));
            isValid = false;
        }

        if (StringUtils.isBlank(salesforceClientSecret) || StringUtils.isNullOrEmpty(salesforceClientSecret)) {
            messages.add(getString(MESSAGE_SALESFORCE_CLIENT_SECRET_REQUIRED));
            isValid = false;
        }

        if (StringUtils.isBlank(salesforceUsername) || StringUtils.isNullOrEmpty(salesforceUsername)) {
            messages.add(getString(MESSAGE_SALESFORCE_ACCOUNT_USERNAME_REQUIRED));
            isValid = false;
        }

        if (StringUtils.isBlank(salesforcePassword) || StringUtils.isNullOrEmpty(salesforcePassword)) {
            messages.add(getString(MESSAGE_SALESFORCE_ACCOUNT_PASSWORD_REQUIRED));
            isValid = false;
        }

        if (StringUtils.isBlank(salesforceSecurityToken) || StringUtils.isNullOrEmpty(salesforceSecurityToken)) {
            messages.add(getString(MESSAGE_SALESFORCE_SECURITY_TOKEN_REQUIRED));
            isValid = false;
        }

        StringBuilder msgString = new StringBuilder();
        messages.forEach((message) -> msgString.append(message).append("\n"));
        if (!isValid) {
            showErrorMessage(getString(MESSAGE_VALIDATION_ERROR), /*getString(MESSAGE_CORRECT_ERRORS) + "\n" +*/ msgString.toString());
        }
        return isValid;
    }

    @Override
    public void showToolTip(String message, int code) {
        switch (code) {
            case MESSAGE_CODES.INFO:
                SianRosesApp.displayInfoMessage(message);
                break;
            case MESSAGE_CODES.WARNING:
                SianRosesApp.displayWarningMessage(message);
                break;
            case MESSAGE_CODES.ERROR:
                SianRosesApp.displayErrorMessage(message);
                break;
            default:
                SianRosesApp.displayOrdinaryMessage(message);
                break;
        }
    }

    @Override
    public void addToProcessStack(String processName) {
        runningProcesses.add(processName);
    }

    @Override
    public void removeFromProcessStack(String processName) {
        runningProcesses.remove(processName);
    }

    @Override
    public void clearProcessStack() {
        runningProcesses.clear();
    }
}
